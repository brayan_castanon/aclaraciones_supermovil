var suiteData = [{"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "096dd151-e86c-434f-85e1-3fd23adc59fb",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/22/2020-10:00:06",
        "dateOnly": "10/22/2020",
        "timeOnly": "10:00:06",
        "raw": 1603378806347
    },
    "endTime": {
        "dateTime": "10/22/2020-10:00:14",
        "dateOnly": "10/22/2020",
        "timeOnly": "10:00:14",
        "raw": 1603378814501
    },
    "folderName": "10-22_10-00-14-031",
    "reportFolder": ".\\resources\\.\\reporte\\10-22_10-00-14-031",
    "executionSummary": [{
        "exceptionType": "CLOUD",
        "ignored": "0",
        "elementUsageMap": {"null.null.resumenTDCAndroid": {
            "failCount": "1",
            "keyName": "null.null.resumenTDCAndroid",
            "siteName": "null",
            "pageName": "null",
            "passCount": "0",
            "elementName": "resumenTDCAndroid"
        }},
        "failed": "1",
        "sessionId": "null",
        "callMap": {},
        "elementUsage": [{
            "failCount": "1",
            "keyName": "null.null.resumenTDCAndroid",
            "siteName": "null",
            "pageName": "null",
            "passCount": "0",
            "elementName": "resumenTDCAndroid"
        }],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "1",
        "testStatus": "FAILED",
        "pageUsageMap": {"null.null": [
            "0",
            "1"
        ]},
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/22/2020-10:00:14",
            "dateOnly": "10/22/2020",
            "timeOnly": "10:00:14",
            "raw": 1603378814016
        },
        "endTime": {
            "dateTime": "10/22/2020-10:00:14",
            "dateOnly": "10/22/2020",
            "timeOnly": "10:00:14",
            "raw": 1603378814021
        },
        "passed": "0",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {
                "appPackage": "com.santander.globile.archetypeandroid",
                "noReset": "false",
                "_testName": "resumenTDCAndroid (flujo 2)",
                "deviceName": "CE0718272911AA3103"
            },
            "os": "Android",
            "active": true,
            "deviceLock": "java.util.concurrent.Semaphore@1071649[Permits = 1]",
            "deviceName": "null",
            "resolution": "null",
            "capList": [
                "org.xframium.device.DeviceCap@8a4fb9",
                "org.xframium.device.DeviceCap@1b8fef",
                "org.xframium.device.DeviceCap@16f657b",
                "org.xframium.device.DeviceCap@318422",
                "org.xframium.device.DeviceCap@8cf25a",
                "org.xframium.device.DeviceCap@386e53"
            ],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Android",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android",
            "phoneNumber": "null",
            "osVersion": "null",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "Android Galaxy S8+ [Galaxy S8+]",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "df260f69-eb28-4446-8e01-6c09bc07c38a",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/22/2020-09:59:12",
        "dateOnly": "10/22/2020",
        "timeOnly": "09:59:12",
        "raw": 1603378752635
    },
    "endTime": {
        "dateTime": "10/22/2020-09:59:25",
        "dateOnly": "10/22/2020",
        "timeOnly": "09:59:25",
        "raw": 1603378765685
    },
    "folderName": "10-22_09-59-24-889",
    "reportFolder": ".\\resources\\.\\reporte\\10-22_09-59-24-889",
    "executionSummary": [{
        "exceptionType": "CLOUD",
        "ignored": "0",
        "elementUsageMap": {"null.null.resumenTDCAndroid": {
            "failCount": "1",
            "keyName": "null.null.resumenTDCAndroid",
            "siteName": "null",
            "pageName": "null",
            "passCount": "0",
            "elementName": "resumenTDCAndroid"
        }},
        "failed": "1",
        "sessionId": "null",
        "callMap": {},
        "elementUsage": [{
            "failCount": "1",
            "keyName": "null.null.resumenTDCAndroid",
            "siteName": "null",
            "pageName": "null",
            "passCount": "0",
            "elementName": "resumenTDCAndroid"
        }],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "1",
        "testStatus": "FAILED",
        "pageUsageMap": {"null.null": [
            "0",
            "1"
        ]},
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/22/2020-09:59:24",
            "dateOnly": "10/22/2020",
            "timeOnly": "09:59:24",
            "raw": 1603378764874
        },
        "endTime": {
            "dateTime": "10/22/2020-09:59:24",
            "dateOnly": "10/22/2020",
            "timeOnly": "09:59:24",
            "raw": 1603378764881
        },
        "passed": "0",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {
                "appPackage": "com.santander.globile.archetypeandroid",
                "noReset": "false",
                "_testName": "resumenTDCAndroid (flujo 2)",
                "deviceName": "CE0718272911AA3103"
            },
            "os": "Android",
            "active": true,
            "deviceLock": "java.util.concurrent.Semaphore@176ad8e[Permits = 1]",
            "deviceName": "null",
            "resolution": "null",
            "capList": [
                "org.xframium.device.DeviceCap@197a97e",
                "org.xframium.device.DeviceCap@132e3de",
                "org.xframium.device.DeviceCap@1a42aeb",
                "org.xframium.device.DeviceCap@1b64272",
                "org.xframium.device.DeviceCap@112d7a3",
                "org.xframium.device.DeviceCap@1daf78c"
            ],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Android",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android",
            "phoneNumber": "null",
            "osVersion": "null",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "Android Galaxy S8+ [Galaxy S8+]",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "1d1d4715-ea11-4680-ac95-735ff9cfb436",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-17:23:37",
        "dateOnly": "10/21/2020",
        "timeOnly": "17:23:37",
        "raw": 1603319017232
    },
    "endTime": {
        "dateTime": "10/21/2020-17:28:14",
        "dateOnly": "10/21/2020",
        "timeOnly": "17:28:14",
        "raw": 1603319294543
    },
    "folderName": "10-21_17-23-49-456",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_17-23-49-456",
    "executionSummary": [{
        "exceptionType": "null",
        "ignored": "0",
        "elementUsageMap": {
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@class='sn-FUNC056']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "0",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_22_23_41_8410",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fncerrarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "1",
                "0"
            ],
            "fnmenuMasAndroid": [
                "1",
                "0"
            ]
        },
        "elementUsage": [
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@class='sn-FUNC056']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "65",
        "testStatus": "PASSED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "22",
                "0"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-17:23:48",
            "dateOnly": "10/21/2020",
            "timeOnly": "17:23:48",
            "raw": 1603319028871
        },
        "endTime": {
            "dateTime": "10/21/2020-17:27:57",
            "dateOnly": "10/21/2020",
            "timeOnly": "17:27:57",
            "raw": 1603319277429
        },
        "passed": "65",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@1e386ae[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "e54bbb8f-0495-4327-a6c2-48dd0e1c17dc",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-16:50:07",
        "dateOnly": "10/21/2020",
        "timeOnly": "16:50:07",
        "raw": 1603317007542
    },
    "endTime": {
        "dateTime": "10/21/2020-16:54:46",
        "dateOnly": "10/21/2020",
        "timeOnly": "16:54:46",
        "raw": 1603317286538
    },
    "folderName": "10-21_16-50-17-552",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_16-50-17-552",
    "executionSummary": [{
        "exceptionType": "null",
        "ignored": "0",
        "elementUsageMap": {
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@class='sn-FUNC056']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "0",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_21_50_09_8394",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fncerrarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "1",
                "0"
            ],
            "fnmenuMasAndroid": [
                "1",
                "0"
            ]
        },
        "elementUsage": [
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@class='sn-FUNC056']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "65",
        "testStatus": "PASSED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "22",
                "0"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-16:50:16",
            "dateOnly": "10/21/2020",
            "timeOnly": "16:50:16",
            "raw": 1603317016691
        },
        "endTime": {
            "dateTime": "10/21/2020-16:54:29",
            "dateOnly": "10/21/2020",
            "timeOnly": "16:54:29",
            "raw": 1603317269227
        },
        "passed": "65",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@1747bbf[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "5d5612db-1d93-4b9b-80e1-59124d154e9b",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-16:42:18",
        "dateOnly": "10/21/2020",
        "timeOnly": "16:42:18",
        "raw": 1603316538465
    },
    "endTime": {
        "dateTime": "10/21/2020-16:46:57",
        "dateOnly": "10/21/2020",
        "timeOnly": "16:46:57",
        "raw": 1603316817511
    },
    "folderName": "10-21_16-42-27-963",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_16-42-27-963",
    "executionSummary": [{
        "exceptionType": "null",
        "ignored": "0",
        "elementUsageMap": {
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@class='sn-FUNC056']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "0",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_21_42_20_8389",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fncerrarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "1",
                "0"
            ],
            "fnmenuMasAndroid": [
                "1",
                "0"
            ]
        },
        "elementUsage": [
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@class='sn-FUNC056']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "65",
        "testStatus": "PASSED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "22",
                "0"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-16:42:27",
            "dateOnly": "10/21/2020",
            "timeOnly": "16:42:27",
            "raw": 1603316547432
        },
        "endTime": {
            "dateTime": "10/21/2020-16:46:40",
            "dateOnly": "10/21/2020",
            "timeOnly": "16:46:40",
            "raw": 1603316800356
        },
        "passed": "65",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@f232bb[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "3faa589b-c009-4113-86c2-417a332d450f",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-16:36:46",
        "dateOnly": "10/21/2020",
        "timeOnly": "16:36:46",
        "raw": 1603316206124
    },
    "endTime": {
        "dateTime": "10/21/2020-16:41:27",
        "dateOnly": "10/21/2020",
        "timeOnly": "16:41:27",
        "raw": 1603316487766
    },
    "folderName": "10-21_16-36-55-403",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_16-36-55-403",
    "executionSummary": [{
        "exceptionType": "null",
        "ignored": "0",
        "elementUsageMap": {
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@class='sn-FUNC056']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "0",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_21_36_48_8383",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fncerrarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "1",
                "0"
            ],
            "fnmenuMasAndroid": [
                "1",
                "0"
            ]
        },
        "elementUsage": [
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@class='sn-FUNC056']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "65",
        "testStatus": "PASSED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "22",
                "0"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-16:36:54",
            "dateOnly": "10/21/2020",
            "timeOnly": "16:36:54",
            "raw": 1603316214842
        },
        "endTime": {
            "dateTime": "10/21/2020-16:41:10",
            "dateOnly": "10/21/2020",
            "timeOnly": "16:41:10",
            "raw": 1603316470871
        },
        "passed": "65",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@1062f53[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "2ddee57b-d93c-45dd-b558-ed524297770c",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-16:31:44",
        "dateOnly": "10/21/2020",
        "timeOnly": "16:31:44",
        "raw": 1603315904923
    },
    "endTime": {
        "dateTime": "10/21/2020-16:34:07",
        "dateOnly": "10/21/2020",
        "timeOnly": "16:34:07",
        "raw": 1603316047631
    },
    "folderName": "10-21_16-31-54-107",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_16-31-54-107",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "1",
        "elementUsageMap": {
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "miVida"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.modoLigth": {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.modoLigth": {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "6",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_21_31_47_8378",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "0",
                "1"
            ]
        },
        "elementUsage": [
            {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "miVida"
            },
            {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "22",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "2",
                "0"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "1"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "2",
                "2"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-16:31:53",
            "dateOnly": "10/21/2020",
            "timeOnly": "16:31:53",
            "raw": 1603315913514
        },
        "endTime": {
            "dateTime": "10/21/2020-16:33:49",
            "dateOnly": "10/21/2020",
            "timeOnly": "16:33:49",
            "raw": 1603316029305
        },
        "passed": "15",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@be2bb4[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "7b156771-902d-458e-af34-8a63dac58c01",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-16:21:17",
        "dateOnly": "10/21/2020",
        "timeOnly": "16:21:17",
        "raw": 1603315277110
    },
    "endTime": {
        "dateTime": "10/21/2020-16:27:43",
        "dateOnly": "10/21/2020",
        "timeOnly": "16:27:43",
        "raw": 1603315663358
    },
    "folderName": "10-21_16-21-26-775",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_16-21-26-775",
    "executionSummary": [{
        "exceptionType": "null",
        "ignored": "0",
        "elementUsageMap": {
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]": {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]": {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']": {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: //*[@class='sn-FUNC056']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]": {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "7",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_21_21_19_8364",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fncerrarSesionAndroid": [
                "0",
                "1"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "1",
                "0"
            ],
            "fnmenuMasAndroid": [
                "1",
                "0"
            ]
        },
        "elementUsage": [
            {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: //*[@class='sn-FUNC056']"
            },
            {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "57",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "14",
                "4"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-16:21:26",
            "dateOnly": "10/21/2020",
            "timeOnly": "16:21:26",
            "raw": 1603315286223
        },
        "endTime": {
            "dateTime": "10/21/2020-16:27:18",
            "dateOnly": "10/21/2020",
            "timeOnly": "16:27:18",
            "raw": 1603315638458
        },
        "passed": "50",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@94d351[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "fd7f798f-c4eb-431a-bcb8-8142e198b530",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-16:09:04",
        "dateOnly": "10/21/2020",
        "timeOnly": "16:09:04",
        "raw": 1603314544013
    },
    "endTime": {
        "dateTime": "10/21/2020-16:15:49",
        "dateOnly": "10/21/2020",
        "timeOnly": "16:15:49",
        "raw": 1603314949820
    },
    "folderName": "10-21_16-09-13-717",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_16-09-13-717",
    "executionSummary": [{
        "exceptionType": "null",
        "ignored": "0",
        "elementUsageMap": {
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]": {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]": {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']": {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: //*[@class='sn-FUNC056']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]": {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "7",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_21_09_06_8351",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fncerrarSesionAndroid": [
                "0",
                "1"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "1",
                "0"
            ],
            "fnmenuMasAndroid": [
                "1",
                "0"
            ]
        },
        "elementUsage": [
            {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: //*[@class='sn-FUNC056']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: //*[@class='sn-FUNC056']"
            },
            {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            {
                "failCount": "1",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "0",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "57",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "14",
                "4"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-16:09:13",
            "dateOnly": "10/21/2020",
            "timeOnly": "16:09:13",
            "raw": 1603314553082
        },
        "endTime": {
            "dateTime": "10/21/2020-16:15:24",
            "dateOnly": "10/21/2020",
            "timeOnly": "16:15:24",
            "raw": 1603314924131
        },
        "passed": "50",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@1f1333[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "7d186eb0-5378-4be2-8259-6dc0d1df98d7",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-15:57:46",
        "dateOnly": "10/21/2020",
        "timeOnly": "15:57:46",
        "raw": 1603313866489
    },
    "endTime": {
        "dateTime": "10/21/2020-16:01:38",
        "dateOnly": "10/21/2020",
        "timeOnly": "16:01:38",
        "raw": 1603314098063
    },
    "folderName": "10-21_15-57-57-081",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_15-57-57-081",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "0",
        "elementUsageMap": {
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "3",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_20_57_49_8345",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fncerrarSesionAndroid": [
                "0",
                "1"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "1",
                "0"
            ]
        },
        "elementUsage": [
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "55",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "20",
                "0"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-15:57:56",
            "dateOnly": "10/21/2020",
            "timeOnly": "15:57:56",
            "raw": 1603313876505
        },
        "endTime": {
            "dateTime": "10/21/2020-16:01:12",
            "dateOnly": "10/21/2020",
            "timeOnly": "16:01:12",
            "raw": 1603314072937
        },
        "passed": "52",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@19b05cf[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "7d9d122d-4de8-4fe2-ad5d-0256f6ab77c1",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-15:42:00",
        "dateOnly": "10/21/2020",
        "timeOnly": "15:42:00",
        "raw": 1603312920109
    },
    "endTime": {
        "dateTime": "10/21/2020-15:45:59",
        "dateOnly": "10/21/2020",
        "timeOnly": "15:45:59",
        "raw": 1603313159060
    },
    "folderName": "10-21_15-42-10-650",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_15-42-10-650",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "0",
        "elementUsageMap": {
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "3",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_20_42_03_8328",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fncerrarSesionAndroid": [
                "0",
                "1"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "1",
                "0"
            ]
        },
        "elementUsage": [
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: (//button[@id=\"aceptar-button\"])[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: (//button[@id=\"aceptar-button\"])[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/select[1]/option[33]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "55",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "20",
                "0"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-15:42:10",
            "dateOnly": "10/21/2020",
            "timeOnly": "15:42:10",
            "raw": 1603312930108
        },
        "endTime": {
            "dateTime": "10/21/2020-15:45:32",
            "dateOnly": "10/21/2020",
            "timeOnly": "15:45:32",
            "raw": 1603313132180
        },
        "passed": "52",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@188b4a0[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "4d915b61-4453-408a-994a-1b8878ca3569",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-14:55:06",
        "dateOnly": "10/21/2020",
        "timeOnly": "14:55:06",
        "raw": 1603310106749
    },
    "endTime": {
        "dateTime": "10/21/2020-14:57:47",
        "dateOnly": "10/21/2020",
        "timeOnly": "14:57:47",
        "raw": 1603310267600
    },
    "folderName": "10-21_14-55-17-160",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_14-55-17-160",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "1",
        "elementUsageMap": {
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "Supermovil.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation.VISIBLE": {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation.VISIBLE",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "lblWhatIsTheSituation.VISIBLE"
            },
            "null.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation": {
                "failCount": "0",
                "keyName": "null.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation",
                "siteName": "null",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "lblWhatIsTheSituation"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "3",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_19_55_09_8318",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "0",
                "1"
            ]
        },
        "elementUsage": [
            {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation.VISIBLE",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "lblWhatIsTheSituation.VISIBLE"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation",
                "siteName": "null",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "lblWhatIsTheSituation"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "31",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "6",
                "0"
            ],
            "null.objectRepositoryResumenTDCAndroid": [
                "0",
                "0"
            ],
            "Supermovil.objectRepositoryResumenTDCAndroid": [
                "0",
                "1"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-14:55:16",
            "dateOnly": "10/21/2020",
            "timeOnly": "14:55:16",
            "raw": 1603310116529
        },
        "endTime": {
            "dateTime": "10/21/2020-14:57:22",
            "dateOnly": "10/21/2020",
            "timeOnly": "14:57:22",
            "raw": 1603310242528
        },
        "passed": "27",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@36a1fb[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "063b99a6-37c9-42d6-94c6-c271df58627e",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-14:08:38",
        "dateOnly": "10/21/2020",
        "timeOnly": "14:08:38",
        "raw": 1603307318775
    },
    "endTime": {
        "dateTime": "10/21/2020-14:11:20",
        "dateOnly": "10/21/2020",
        "timeOnly": "14:11:20",
        "raw": 1603307480009
    },
    "folderName": "10-21_14-08-51-467",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_14-08-51-467",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "1",
        "elementUsageMap": {
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "Supermovil.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation.VISIBLE": {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation.VISIBLE",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "lblWhatIsTheSituation.VISIBLE"
            },
            "null.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation": {
                "failCount": "0",
                "keyName": "null.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation",
                "siteName": "null",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "lblWhatIsTheSituation"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "3",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_19_08_41_8301",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "0",
                "1"
            ]
        },
        "elementUsage": [
            {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation.VISIBLE",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "lblWhatIsTheSituation.VISIBLE"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation",
                "siteName": "null",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "lblWhatIsTheSituation"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "31",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "6",
                "0"
            ],
            "null.objectRepositoryResumenTDCAndroid": [
                "0",
                "0"
            ],
            "Supermovil.objectRepositoryResumenTDCAndroid": [
                "0",
                "1"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-14:08:50",
            "dateOnly": "10/21/2020",
            "timeOnly": "14:08:50",
            "raw": 1603307330843
        },
        "endTime": {
            "dateTime": "10/21/2020-14:10:55",
            "dateOnly": "10/21/2020",
            "timeOnly": "14:10:55",
            "raw": 1603307455796
        },
        "passed": "27",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@36a1fb[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "d9d91939-39f8-48db-a775-058ac638770e",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-13:54:54",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:54:54",
        "raw": 1603306494199
    },
    "endTime": {
        "dateTime": "10/21/2020-13:57:31",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:57:31",
        "raw": 1603306651447
    },
    "folderName": "10-21_13-55-04-665",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_13-55-04-665",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "0",
        "elementUsageMap": {
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "Supermovil.objectRepositoryResumenTDCAndroid.BtnClarifyMovement": {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.BtnClarifyMovement",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "BtnClarifyMovement"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "3",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_18_54_57_8298",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "0",
                "1"
            ]
        },
        "elementUsage": [
            {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.BtnClarifyMovement",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "BtnClarifyMovement"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "27",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "4",
                "0"
            ],
            "Supermovil.objectRepositoryResumenTDCAndroid": [
                "0",
                "1"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-13:55:04",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:55:04",
            "raw": 1603306504151
        },
        "endTime": {
            "dateTime": "10/21/2020-13:57:06",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:57:06",
            "raw": 1603306626877
        },
        "passed": "24",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@10c01d2[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "49c0b363-39ac-47a7-aeaf-0d3fa6cec5cd",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-13:53:00",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:53:00",
        "raw": 1603306380589
    },
    "endTime": {
        "dateTime": "10/21/2020-13:54:18",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:54:18",
        "raw": 1603306458688
    },
    "folderName": "10-21_13-53-10-899",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_13-53-10-899",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "0",
        "elementUsageMap": {
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            }
        },
        "failed": "3",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_18_53_02_8295",
        "callMap": {"fniniciarSesionAndroid": [
            "0",
            "1"
        ]},
        "elementUsage": [
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "7",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-13:53:10",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:53:10",
            "raw": 1603306390257
        },
        "endTime": {
            "dateTime": "10/21/2020-13:53:53",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:53:53",
            "raw": 1603306433897
        },
        "passed": "4",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@1691d1[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "713a5e07-b832-475a-b714-f70b3455765b",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-13:33:11",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:33:11",
        "raw": 1603305191733
    },
    "endTime": {
        "dateTime": "10/21/2020-13:35:42",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:35:42",
        "raw": 1603305342576
    },
    "folderName": "10-21_13-33-22-994",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_13-33-22-994",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "0",
        "elementUsageMap": {
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "Supermovil.objectRepositoryResumenTDCAndroid.resumenTDD": {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.resumenTDD",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "resumenTDD"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "3",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_18_33_13_8286",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "0",
                "1"
            ]
        },
        "elementUsage": [
            {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.resumenTDD",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "resumenTDD"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "24",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "2",
                "0"
            ],
            "Supermovil.objectRepositoryResumenTDCAndroid": [
                "0",
                "1"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-13:33:22",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:33:22",
            "raw": 1603305202482
        },
        "endTime": {
            "dateTime": "10/21/2020-13:35:18",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:35:18",
            "raw": 1603305318241
        },
        "passed": "21",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@baf474[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "7ce80c63-7225-4c9b-9545-3850dfafb29f",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-13:30:10",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:30:10",
        "raw": 1603305010200
    },
    "endTime": {
        "dateTime": "10/21/2020-13:32:43",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:32:43",
        "raw": 1603305163362
    },
    "folderName": "10-21_13-30-19-690",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_13-30-19-690",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "1",
        "elementUsageMap": {
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "miVida"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.modoLigth": {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.modoLigth": {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "6",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_18_30_12_8283",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "0",
                "1"
            ]
        },
        "elementUsage": [
            {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "miVida"
            },
            {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "22",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "2",
                "0"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "1"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "2",
                "2"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-13:30:19",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:30:19",
            "raw": 1603305019128
        },
        "endTime": {
            "dateTime": "10/21/2020-13:32:22",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:32:22",
            "raw": 1603305142457
        },
        "passed": "15",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@1016ed0[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "6c8fe11e-f13f-4d93-8f64-a35f35a5dcd5",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-13:26:33",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:26:33",
        "raw": 1603304793748
    },
    "endTime": {
        "dateTime": "10/21/2020-13:29:12",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:29:12",
        "raw": 1603304952701
    },
    "folderName": "10-21_13-26-46-378",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_13-26-46-378",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "1",
        "elementUsageMap": {
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "miVida"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.modoLigth": {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.modoLigth": {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "6",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_18_26_35_8280",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "0",
                "1"
            ]
        },
        "elementUsage": [
            {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "miVida"
            },
            {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "22",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "2",
                "0"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "1"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "2",
                "2"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-13:26:45",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:26:45",
            "raw": 1603304805792
        },
        "endTime": {
            "dateTime": "10/21/2020-13:28:54",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:28:54",
            "raw": 1603304934345
        },
        "passed": "15",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@1194392[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "7ece129c-0310-4542-a75f-b49fd860e680",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-13:20:59",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:20:59",
        "raw": 1603304459895
    },
    "endTime": {
        "dateTime": "10/21/2020-13:23:42",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:23:42",
        "raw": 1603304622522
    },
    "folderName": "10-21_13-21-09-663",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_13-21-09-663",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "0",
        "elementUsageMap": {
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "Supermovil.objectRepositoryResumenTDCAndroid.resumenTDD": {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.resumenTDD",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "resumenTDD"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "3",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_18_21_01_8275",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "0",
                "1"
            ]
        },
        "elementUsage": [
            {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.resumenTDD",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "resumenTDD"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "24",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "2",
                "0"
            ],
            "Supermovil.objectRepositoryResumenTDCAndroid": [
                "0",
                "1"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-13:21:09",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:21:09",
            "raw": 1603304469101
        },
        "endTime": {
            "dateTime": "10/21/2020-13:23:17",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:23:17",
            "raw": 1603304597639
        },
        "passed": "21",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@1be2695[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "d1769d39-4887-4867-b121-67b98e853d7e",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-12:57:59",
        "dateOnly": "10/21/2020",
        "timeOnly": "12:57:59",
        "raw": 1603303079498
    },
    "endTime": {
        "dateTime": "10/21/2020-13:00:53",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:00:53",
        "raw": 1603303253272
    },
    "folderName": "10-21_12-58-14-414",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_12-58-14-414",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "0",
        "elementUsageMap": {
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "Supermovil.objectRepositoryResumenTDCAndroid.resumenTDD": {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.resumenTDD",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "resumenTDD"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "3",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_17_58_01_8267",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "0",
                "1"
            ]
        },
        "elementUsage": [
            {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.resumenTDD",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "resumenTDD"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "23",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "2",
                "0"
            ],
            "Supermovil.objectRepositoryResumenTDCAndroid": [
                "0",
                "1"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-12:58:13",
            "dateOnly": "10/21/2020",
            "timeOnly": "12:58:13",
            "raw": 1603303093818
        },
        "endTime": {
            "dateTime": "10/21/2020-13:00:26",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:00:26",
            "raw": 1603303226485
        },
        "passed": "20",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@5fa84a[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "c6941b44-69e4-43e4-9131-b73ed1b1154d",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-12:45:23",
        "dateOnly": "10/21/2020",
        "timeOnly": "12:45:23",
        "raw": 1603302323078
    },
    "endTime": {
        "dateTime": "10/21/2020-12:47:37",
        "dateOnly": "10/21/2020",
        "timeOnly": "12:47:37",
        "raw": 1603302457069
    },
    "folderName": "10-21_12-45-33-693",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_12-45-33-693",
    "executionSummary": [{
        "exceptionType": "CONFIGURATION",
        "ignored": "0",
        "elementUsageMap": {
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "2",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_17_45_25_8257",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "0",
                "1"
            ]
        },
        "elementUsage": [
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "22",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "2",
                "0"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-12:45:33",
            "dateOnly": "10/21/2020",
            "timeOnly": "12:45:33",
            "raw": 1603302333121
        },
        "endTime": {
            "dateTime": "10/21/2020-12:47:13",
            "dateOnly": "10/21/2020",
            "timeOnly": "12:47:13",
            "raw": 1603302433252
        },
        "passed": "20",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@783a42[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}}, {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "1ce3947f-99c5-4b56-9dc7-db04c0b04ce5",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-12:39:52",
        "dateOnly": "10/21/2020",
        "timeOnly": "12:39:52",
        "raw": 1603301992189
    },
    "endTime": {
        "dateTime": "10/21/2020-12:42:47",
        "dateOnly": "10/21/2020",
        "timeOnly": "12:42:47",
        "raw": 1603302167990
    },
    "folderName": "10-21_12-40-08-471",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_12-40-08-471",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "1",
        "elementUsageMap": {
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "miVida"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.modoLigth": {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.modoLigth": {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "6",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_17_39_55_8250",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "0",
                "1"
            ]
        },
        "elementUsage": [
            {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "miVida"
            },
            {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "22",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "2",
                "0"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "1"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "2",
                "2"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-12:40:07",
            "dateOnly": "10/21/2020",
            "timeOnly": "12:40:07",
            "raw": 1603302007447
        },
        "endTime": {
            "dateTime": "10/21/2020-12:42:26",
            "dateOnly": "10/21/2020",
            "timeOnly": "12:42:26",
            "raw": 1603302146408
        },
        "passed": "15",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@aa9671[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}},  ];