var testData = {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "a0d82a57-4a1e-46a1-9135-457c718c44e4",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-10:50:12",
        "dateOnly": "10/21/2020",
        "timeOnly": "10:50:12",
        "raw": 1603295412425
    },
    "endTime": {
        "dateTime": "10/21/2020-10:51:32",
        "dateOnly": "10/21/2020",
        "timeOnly": "10:51:32",
        "raw": 1603295492965
    },
    "folderName": "10-21_10-51-32-315",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_10-51-32-315",
    "executionSummary": [{
        "exceptionType": "CLOUD",
        "ignored": "0",
        "elementUsageMap": {"null.null.resumenTDCAndroid": {
            "failCount": "1",
            "keyName": "null.null.resumenTDCAndroid",
            "siteName": "null",
            "pageName": "null",
            "passCount": "0",
            "elementName": "resumenTDCAndroid"
        }},
        "failed": "1",
        "sessionId": "null",
        "callMap": {},
        "elementUsage": [{
            "failCount": "1",
            "keyName": "null.null.resumenTDCAndroid",
            "siteName": "null",
            "pageName": "null",
            "passCount": "0",
            "elementName": "resumenTDCAndroid"
        }],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "1",
        "testStatus": "FAILED",
        "pageUsageMap": {"null.null": [
            "0",
            "1"
        ]},
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-10:51:32",
            "dateOnly": "10/21/2020",
            "timeOnly": "10:51:32",
            "raw": 1603295492305
        },
        "endTime": {
            "dateTime": "10/21/2020-10:51:32",
            "dateOnly": "10/21/2020",
            "timeOnly": "10:51:32",
            "raw": 1603295492309
        },
        "passed": "0",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {
                "appPackage": "com.santander.mx.nuevasupermovil",
                "noReset": "false",
                "_testName": "resumenTDCAndroid (flujo 2)",
                "deviceName": "CE0718272911AA3103"
            },
            "os": "Android",
            "active": true,
            "deviceLock": "java.util.concurrent.Semaphore@1502c09[Permits = 1]",
            "deviceName": "null",
            "resolution": "null",
            "capList": [
                "org.xframium.device.DeviceCap@11e3b37",
                "org.xframium.device.DeviceCap@6a1c6a",
                "org.xframium.device.DeviceCap@6c464a",
                "org.xframium.device.DeviceCap@4e767b",
                "org.xframium.device.DeviceCap@e73d86",
                "org.xframium.device.DeviceCap@4cfa3c"
            ],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Android",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android",
            "phoneNumber": "null",
            "osVersion": "null",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "Android Galaxy S8+ [Galaxy S8+]",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}};