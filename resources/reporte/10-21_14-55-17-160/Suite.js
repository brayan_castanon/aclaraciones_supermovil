var testData = {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "4d915b61-4453-408a-994a-1b8878ca3569",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-14:55:06",
        "dateOnly": "10/21/2020",
        "timeOnly": "14:55:06",
        "raw": 1603310106749
    },
    "endTime": {
        "dateTime": "10/21/2020-14:57:47",
        "dateOnly": "10/21/2020",
        "timeOnly": "14:57:47",
        "raw": 1603310267600
    },
    "folderName": "10-21_14-55-17-160",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_14-55-17-160",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "1",
        "elementUsageMap": {
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "Supermovil.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation.VISIBLE": {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation.VISIBLE",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "lblWhatIsTheSituation.VISIBLE"
            },
            "null.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation": {
                "failCount": "0",
                "keyName": "null.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation",
                "siteName": "null",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "lblWhatIsTheSituation"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "3",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_19_55_09_8318",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "0",
                "1"
            ]
        },
        "elementUsage": [
            {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation.VISIBLE",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "lblWhatIsTheSituation.VISIBLE"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryResumenTDCAndroid.lblWhatIsTheSituation",
                "siteName": "null",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "lblWhatIsTheSituation"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "31",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "6",
                "0"
            ],
            "null.objectRepositoryResumenTDCAndroid": [
                "0",
                "0"
            ],
            "Supermovil.objectRepositoryResumenTDCAndroid": [
                "0",
                "1"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-14:55:16",
            "dateOnly": "10/21/2020",
            "timeOnly": "14:55:16",
            "raw": 1603310116529
        },
        "endTime": {
            "dateTime": "10/21/2020-14:57:22",
            "dateOnly": "10/21/2020",
            "timeOnly": "14:57:22",
            "raw": 1603310242528
        },
        "passed": "27",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@36a1fb[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}};