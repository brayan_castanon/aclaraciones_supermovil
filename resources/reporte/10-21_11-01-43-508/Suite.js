var testData = {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "9b2a60eb-42d5-4f1d-a6b2-e2475df3cc4e",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-11:01:31",
        "dateOnly": "10/21/2020",
        "timeOnly": "11:01:31",
        "raw": 1603296091227
    },
    "endTime": {
        "dateTime": "10/21/2020-11:05:57",
        "dateOnly": "10/21/2020",
        "timeOnly": "11:05:57",
        "raw": 1603296357875
    },
    "folderName": "10-21_11-01-43-508",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_11-01-43-508",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "1",
        "elementUsageMap": {
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryResumenTDCAndroid.leyendaCuentasTDC": {
                "failCount": "2",
                "keyName": "null.objectRepositoryResumenTDCAndroid.leyendaCuentasTDC",
                "siteName": "null",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "leyendaCuentasTDC"
            },
            "Supermovil.objectRepositoryResumenTDCAndroid.leyendaCuentasTDC.VISIBLE": {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.leyendaCuentasTDC.VISIBLE",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "leyendaCuentasTDC.VISIBLE"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "5",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_16_01_33_8134",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "0",
                "1"
            ]
        },
        "elementUsage": [
            {
                "failCount": "2",
                "keyName": "null.objectRepositoryResumenTDCAndroid.leyendaCuentasTDC",
                "siteName": "null",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "leyendaCuentasTDC"
            },
            {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.leyendaCuentasTDC.VISIBLE",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "leyendaCuentasTDC.VISIBLE"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "35",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "2",
                "0"
            ],
            "null.objectRepositoryResumenTDCAndroid": [
                "0",
                "2"
            ],
            "Supermovil.objectRepositoryResumenTDCAndroid": [
                "0",
                "1"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-11:01:42",
            "dateOnly": "10/21/2020",
            "timeOnly": "11:01:42",
            "raw": 1603296102948
        },
        "endTime": {
            "dateTime": "10/21/2020-11:05:32",
            "dateOnly": "10/21/2020",
            "timeOnly": "11:05:32",
            "raw": 1603296332521
        },
        "passed": "29",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@11fe325[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}};