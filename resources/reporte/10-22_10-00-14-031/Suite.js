var testData = {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "096dd151-e86c-434f-85e1-3fd23adc59fb",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/22/2020-10:00:06",
        "dateOnly": "10/22/2020",
        "timeOnly": "10:00:06",
        "raw": 1603378806347
    },
    "endTime": {
        "dateTime": "10/22/2020-10:00:14",
        "dateOnly": "10/22/2020",
        "timeOnly": "10:00:14",
        "raw": 1603378814501
    },
    "folderName": "10-22_10-00-14-031",
    "reportFolder": ".\\resources\\.\\reporte\\10-22_10-00-14-031",
    "executionSummary": [{
        "exceptionType": "CLOUD",
        "ignored": "0",
        "elementUsageMap": {"null.null.resumenTDCAndroid": {
            "failCount": "1",
            "keyName": "null.null.resumenTDCAndroid",
            "siteName": "null",
            "pageName": "null",
            "passCount": "0",
            "elementName": "resumenTDCAndroid"
        }},
        "failed": "1",
        "sessionId": "null",
        "callMap": {},
        "elementUsage": [{
            "failCount": "1",
            "keyName": "null.null.resumenTDCAndroid",
            "siteName": "null",
            "pageName": "null",
            "passCount": "0",
            "elementName": "resumenTDCAndroid"
        }],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "1",
        "testStatus": "FAILED",
        "pageUsageMap": {"null.null": [
            "0",
            "1"
        ]},
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/22/2020-10:00:14",
            "dateOnly": "10/22/2020",
            "timeOnly": "10:00:14",
            "raw": 1603378814016
        },
        "endTime": {
            "dateTime": "10/22/2020-10:00:14",
            "dateOnly": "10/22/2020",
            "timeOnly": "10:00:14",
            "raw": 1603378814021
        },
        "passed": "0",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {
                "appPackage": "com.santander.globile.archetypeandroid",
                "noReset": "false",
                "_testName": "resumenTDCAndroid (flujo 2)",
                "deviceName": "CE0718272911AA3103"
            },
            "os": "Android",
            "active": true,
            "deviceLock": "java.util.concurrent.Semaphore@1071649[Permits = 1]",
            "deviceName": "null",
            "resolution": "null",
            "capList": [
                "org.xframium.device.DeviceCap@8a4fb9",
                "org.xframium.device.DeviceCap@1b8fef",
                "org.xframium.device.DeviceCap@16f657b",
                "org.xframium.device.DeviceCap@318422",
                "org.xframium.device.DeviceCap@8cf25a",
                "org.xframium.device.DeviceCap@386e53"
            ],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Android",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android",
            "phoneNumber": "null",
            "osVersion": "null",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "Android Galaxy S8+ [Galaxy S8+]",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}};