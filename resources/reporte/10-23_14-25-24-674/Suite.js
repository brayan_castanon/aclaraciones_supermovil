var testData = {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "f90590bb-5c29-4a1d-895f-faa59cff3a16",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/23/2020-14:25:16",
        "dateOnly": "10/23/2020",
        "timeOnly": "14:25:16",
        "raw": 1603481116068
    },
    "endTime": {
        "dateTime": "10/23/2020-14:25:25",
        "dateOnly": "10/23/2020",
        "timeOnly": "14:25:25",
        "raw": 1603481125619
    },
    "folderName": "10-23_14-25-24-674",
    "reportFolder": ".\\resources\\.\\reporte\\10-23_14-25-24-674",
    "executionSummary": [{
        "exceptionType": "CLOUD",
        "ignored": "0",
        "elementUsageMap": {"null.null.FraudNationalUnifolioTDDAndroid": {
            "failCount": "1",
            "keyName": "null.null.FraudNationalUnifolioTDDAndroid",
            "siteName": "null",
            "pageName": "null",
            "passCount": "0",
            "elementName": "FraudNationalUnifolioTDDAndroid"
        }},
        "failed": "1",
        "sessionId": "null",
        "callMap": {},
        "elementUsage": [{
            "failCount": "1",
            "keyName": "null.null.FraudNationalUnifolioTDDAndroid",
            "siteName": "null",
            "pageName": "null",
            "passCount": "0",
            "elementName": "FraudNationalUnifolioTDDAndroid"
        }],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "1",
        "testStatus": "FAILED",
        "pageUsageMap": {"null.null": [
            "0",
            "1"
        ]},
        "name": "FraudNationalUnifolioTDDAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/23/2020-14:25:24",
            "dateOnly": "10/23/2020",
            "timeOnly": "14:25:24",
            "raw": 1603481124657
        },
        "endTime": {
            "dateTime": "10/23/2020-14:25:24",
            "dateOnly": "10/23/2020",
            "timeOnly": "14:25:24",
            "raw": 1603481124664
        },
        "passed": "0",
        "folderName": "Galaxy S8+ Android\\FraudNationalUnifolioTDDAndroid (flujo 2)",
        "device": {
            "capabilities": {
                "appPackage": "com.santander.globile.archetypeandroid",
                "noReset": "false",
                "_testName": "FraudNationalUnifolioTDDAndroid (flujo 2)",
                "deviceName": "CE0718272911AA3103"
            },
            "os": "Android",
            "active": true,
            "deviceLock": "java.util.concurrent.Semaphore@1deffb4[Permits = 1]",
            "deviceName": "null",
            "resolution": "null",
            "capList": [
                "org.xframium.device.DeviceCap@1f5459",
                "org.xframium.device.DeviceCap@c5e82d",
                "org.xframium.device.DeviceCap@ac86de",
                "org.xframium.device.DeviceCap@26d95d",
                "org.xframium.device.DeviceCap@2fe81c",
                "org.xframium.device.DeviceCap@114eab4"
            ],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Android",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android",
            "phoneNumber": "null",
            "osVersion": "null",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "Android Galaxy S8+ [Galaxy S8+]",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}};