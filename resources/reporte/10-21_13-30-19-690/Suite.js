var testData = {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "7ce80c63-7225-4c9b-9545-3850dfafb29f",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-13:30:10",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:30:10",
        "raw": 1603305010200
    },
    "endTime": {
        "dateTime": "10/21/2020-13:32:43",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:32:43",
        "raw": 1603305163362
    },
    "folderName": "10-21_13-30-19-690",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_13-30-19-690",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "1",
        "elementUsageMap": {
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "miVida"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.modoLigth": {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.modoLigth": {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "6",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_18_30_12_8283",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "0",
                "1"
            ]
        },
        "elementUsage": [
            {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "miVida"
            },
            {
                "failCount": "1",
                "keyName": "null.objectRepositoryInicioSesionAndroid.modoLigth",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "0",
                "elementName": "modoLigth"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "22",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "2",
                "0"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "1"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "2",
                "2"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-13:30:19",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:30:19",
            "raw": 1603305019128
        },
        "endTime": {
            "dateTime": "10/21/2020-13:32:22",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:32:22",
            "raw": 1603305142457
        },
        "passed": "15",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@1016ed0[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}};