var testData = {"pageData": {
    "phase": "null",
    "aut": "null",
    "sPMap": {},
    "gridUrl": "Grid.html",
    "testTags": ["regresionCiclo1"],
    "domain": "null",
    "configProperties": {
        "xF-ID": "d9d91939-39f8-48db-a775-058ac638770e",
        "pageManagement.pageData.tabNames": "SearchData1,SearchData2,SearchData3,SearchData4,SearchData5"
    },
    "startTime": {
        "dateTime": "10/21/2020-13:54:54",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:54:54",
        "raw": 1603306494199
    },
    "endTime": {
        "dateTime": "10/21/2020-13:57:31",
        "dateOnly": "10/21/2020",
        "timeOnly": "13:57:31",
        "raw": 1603306651447
    },
    "folderName": "10-21_13-55-04-665",
    "reportFolder": ".\\resources\\.\\reporte\\10-21_13-55-04-665",
    "executionSummary": [{
        "exceptionType": "SCRIPT",
        "ignored": "0",
        "elementUsageMap": {
            "null.objectRepositoryInicioSesionAndroid.miVida": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            },
            "Supermovil.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "Supermovil.objectRepositoryResumenTDCAndroid.BtnClarifyMovement": {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.BtnClarifyMovement",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "BtnClarifyMovement"
            },
            "null.SELENIUM.By.xpath: //*[@id='userPassword']": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]": {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            "null.objectRepositoryInicioSesionAndroid.ingresar": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            "null.objectRepositoryInicioSesionAndroid.boxPassword": {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            }
        },
        "failed": "3",
        "sessionId": "brayan.castanon@gft.com_RemoteWebDriver_20-10-21_18_54_57_8298",
        "callMap": {
            "fniniciarSesionAndroid": [
                "1",
                "0"
            ],
            "fnmisProductosAndroid": [
                "1",
                "0"
            ],
            "fnresumenTDCAndroid": [
                "0",
                "1"
            ]
        },
        "elementUsage": [
            {
                "failCount": "1",
                "keyName": "Supermovil.objectRepositoryResumenTDCAndroid.BtnClarifyMovement",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryResumenTDCAndroid",
                "passCount": "0",
                "elementName": "BtnClarifyMovement"
            },
            {
                "failCount": "0",
                "keyName": "Supermovil.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "Supermovil",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: //*[@id='userPassword']",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: //*[@id='userPassword']"
            },
            {
                "failCount": "0",
                "keyName": "null.SELENIUM.By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]",
                "siteName": "null",
                "pageName": "SELENIUM",
                "passCount": "2",
                "elementName": "By.xpath: /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.boxPassword",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "boxPassword"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.ingresar",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "1",
                "elementName": "ingresar"
            },
            {
                "failCount": "0",
                "keyName": "null.objectRepositoryInicioSesionAndroid.miVida",
                "siteName": "null",
                "pageName": "objectRepositoryInicioSesionAndroid",
                "passCount": "3",
                "elementName": "miVida"
            }
        ],
        "tagNames": ["regresionCiclo1"],
        "cloud": {
            "hostName": "santander-mx.perfectomobile.com",
            "proxyPort": "null",
            "password": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkMTEzMWI5Yi0xMTM2LTRmNjYtOGZiOC0xNjVkZmNiYjliNzEifQ.eyJqdGkiOiJmM2I2MGI5ZS1kNmU4LTQ0YzEtOTllZC1kZDI3NTMxOWQxMjAiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTgzNzcwODM1LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL3NhbnRhbmRlci1teC1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI3YmIzMzliNC1lZGJlLTRmYzQtYjdmNS1hOTU1ODhkMjNlNDciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImIxOTQ0ODUwLWVhY2ItNDIyNi04YmU3LWMzOTQ0ZDY3MGI4NCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjQ5NTlmYjFmLTE3YzMtNDkwZC04MzY5LWExYjU1MmU0YTViZSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgb2ZmbGluZV9hY2Nlc3MifQ.4iuqJWjB6cowhyJE2dAz4LE-O55XRaWNWKPgJqmOCgo",
            "keepAlive": "null",
            "deviceAction": "null",
            "provider": "PERFECTO",
            "gridInstance": "null",
            "name": "perfecto",
            "description": "",
            "userName": "",
            "proxyHost": "",
            "gesture": "null"
        },
        "total": "27",
        "testStatus": "FAILED",
        "pageUsageMap": {
            "null.SELENIUM": [
                "4",
                "0"
            ],
            "Supermovil.objectRepositoryResumenTDCAndroid": [
                "0",
                "1"
            ],
            "Supermovil.objectRepositoryInicioSesionAndroid": [
                "1",
                "0"
            ],
            "null.objectRepositoryInicioSesionAndroid": [
                "5",
                "0"
            ]
        },
        "name": "resumenTDCAndroid (flujo 2)",
        "startTime": {
            "dateTime": "10/21/2020-13:55:04",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:55:04",
            "raw": 1603306504151
        },
        "endTime": {
            "dateTime": "10/21/2020-13:57:06",
            "dateOnly": "10/21/2020",
            "timeOnly": "13:57:06",
            "raw": 1603306626877
        },
        "passed": "24",
        "folderName": "Galaxy S8+ Android\\resumenTDCAndroid (flujo 2)",
        "device": {
            "capabilities": {},
            "os": "Android",
            "active": false,
            "deviceLock": "java.util.concurrent.Semaphore@10c01d2[Permits = 1]",
            "deviceName": "CE0718272911AA3103",
            "resolution": "1440x2960",
            "capList": [],
            "driverType": "ANDROID",
            "tagNames": [],
            "manufacturer": "Samsung",
            "cloud": "perfecto",
            "environment": "Galaxy S8+ Android 9",
            "phoneNumber": "",
            "osVersion": "9",
            "browserVersion": "null",
            "availableDevices": "1",
            "browserName": "null",
            "model": "Galaxy S8+",
            "cachedString": "null",
            "key": "Galaxy S8+"
        }
    }],
    "suiteName": "Supermovil"
}};