package mx.com.santander;

public class ElementosDOM {
	
	
	
	
	
	//IOS
	
//	Click para borrar el buc que ya est� escrito
	public String BTN_BORRAR_BUC = "//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[4]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeKeyboard[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeKey[28]";

//	Android

	public String C_BTN_INGRESAR = "//*[@text='Ingresar']";
	public String C_RESUMEN_TDC = " /html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[2]/div[1]/div[3]/sn-product[1]/div[1]/div[1]/span[3]";
	public String C_BTN_PAGA_TARJETA = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[6]/div[2]/div[1]/sm-credit-card-slider-buttons[1]/div[1]/sn-icon-button[1]/div[1]/div[1]/div[1]";
	public String C_BTN_PAGA_SERVICIOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[6]/div[2]/div[1]/sm-credit-card-slider-buttons[1]/div[1]/sn-icon-button[2]/div[1]/div[1]/div[1]";
	public String C_BTN_DISPON_EFECTIVO = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[6]/div[2]/div[1]/sm-credit-card-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]";

	public String C_CLICK_FILTRAR_TDD = "//*[@text='Filtrar']";

	public String C_CLICK_AGREGAR_CONTACTO = "//*[@class='sn-button-large']";
	public String C_CLICK_CERRAR_COMPROBANTE = "//*[@class='sn-SMOV011']";
	public String C_COMPROBANTE = "//*[text()=\'�Agregaste un contacto!\']";
	public String C_CLICK_CERRAR_MODAL = "Cerrar_Modal";
	public String C_CLICK_VOLVER_MAS = "//*[@class=\"sn-SMOV004\"]";
	public String C_LBL_SIN_MOV = "//*[text()=\'�Nada que mostrarte!\']";
	public String C_BTB_TARJETATERCEROS="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payments-tdc-others-view[1]/div[2]/div[1]/div[1]/div[3]/sn-tag[1]/div[1]/div[1]";

	
	
//	Fer mejora TDD 24/03/20
	public String C_LBL_ULTIMOSMOVIMI_Android ="//*[text()=\"�ltimos movimientos\"]";
	public String C_CLICK_MOVIMIENTO_TDD_Android = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[3]/div[1]/div[3]/div[1]/div[1]/span[3]";
	public String C_LBL_DETALLEDEMOVIMI_Android ="/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[1]/h1[1]";

	
	public String C_LBL_SIN_MOVI = "//*[text()=\"�Nada que mostrarte!\"]";
	public String C_CLICK_CERRARDMOV_TDD_Android = "(//span[@id=\"Cerrar_Modal\"])[1]";

	
	
	public String C_CLICK_PASADO_TDD = "//*[@text='Pasado']";
	public String C_CLICK_MOVPAS_TDD = "//*[@text='08/Jul/19']";
	public String C_CLICK_ACTUAL_TDD = "//*[@text='Actual']";
	public String C_CLICK_DEPOSITOS_TDD = "//*[@text='Dep�sitos']";
	public String C_CLICK_ANTERIOR_TDD = "//*[@text='Anterior']";
	public String C_CLICK_DEPPASADO_TDD = "//*[@text='Dep�sitos']";
	public String C_CLICK_DEPANTERIOR_TDD = "//*[@text='Dep�sitos']";
	public String C_BTN_MOV_DEP_ANT_TDD = "//*[@text='27/Jun/19']";
	public String C_BTN_MOV_DEP_PAS_TDD = "//*[@text='08/Jul/19']";
	public String C_CLICK_MOVIMIENTO_TDD = "Movimiento_Abono_0";

	public String C_CLICK_ABONO_TDD = "//*[@text='05/Ago/19']";
	public String C_CLICK_CERRAR_TDD = "//*[@text='Cerrar']";

	public String C_BTN_FILTRAR_TDD = "//*[@text='Filtrar']";
	public String C_BTN_ACTUAL_TDD = "//*[text()=\"Actual\"]";
	public String C_BTN_PASADO_TDD = "//*[@text='Pasado']";
	public String C_BTN_ANTERIOR_TDD = "//*[@text='Anterior']";
	public String C_BTN_TODOS_TDD = "//*[@text='Todos']";
	public String C_BTN_DEPOSITOS_TDD = "//*[@text='Dep�sitos']";
	public String C_BTN_RETIROS_TDD = "//*[@text='Retiros']";
	public String C_BTN_DETALLE_ACTUAL_TDD = "//*[@text='01/Ago/19']";
	public String C_BTN_DETALLE_PASADO_TDD = "//*[@text='27/Jun/19']";
	public String C_BTN_DETALLE_ANTERIOR_TDD = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div[1]/span[3]";
	public String C_BTN_CERRAR_TDD = "//*[@text='Cerrar']";
	public String DETALLETDD = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[3]";
	public String C_CLICK_PRODUCTO = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[2]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]";
	public String C_CLICK_VOLVER = "/html[1]/body[1]/sm-root[1]/div[1]/div[2]/sn-navbar[1]/footer[1]/div[3]/div[2]";
	public String C_CLICK_PRODUCTO_TDC = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[2]/div[2]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]";
	public String C_BTN_MAS = "//*[@class='sn-FUNC056']";
	public String C_BTN_CERRAR = "//*[text()=\"Cerrar Sesi�n\"]";
	public String C_BTN_POP_ACEPTO = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog-content[1]/div[1]/div[3]/sm-dialog-close-login[1]/div[1]/button[1]";
	public String C_CLICK_CERRAR_VAYA = "/html[1]/body[1]/div[1]/div[8]/div[1]/sn-dialog[1]/div[1]/div[1]/button[1]";
	
	// Fer Android

	public String C_LBL_ULTIMOSMOVIMIENTOS_Android = "//*[@text=\\'�ltimos movimientos\\']";
	public String C_LBL_DETALLEDEMOVIMIENTO_Android = "//*[@text=\"�ltimos movimientos\"]";
	public String C_CLICK_MOVIMIENTO_TDC_Android = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[6]/div[3]/div[1]/div[3]/div[1]/div[1]/span[3]";
	public String C_CLICK_CERRARDMOV_TDC_Android = "//*[@class=\"sn-modal-close-text\"]";
	public String C_BTN_CONTSER_ANDROID = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-more-main-menu-view[1]/div[1]/div[2]/sm-button-list[1]/div[1]/div[2]/span[1]";
	public String C_BTN_RegresarA = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiary-edit[1]/div[1]/div[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]";
	public String C_BTN_RegresarB = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]";
	public String C_BTN_Productos = "/html[1]/body[1]/sm-root[1]/div[1]/div[2]/sn-navbar[1]/footer[1]/div[3]/sn-icon[1]/i[1]";
//	DETALLE DE TDC 

	public String C_BTN_SUPERTOKEN = "//*[@resource-id=\'mx.bancosantander.supermovil:id/et_token_type\']";

//	iOS
	public String C_LBL_TDC_iOS = "//*[@label='Tarjetas de cr�dito']";
	public String C_RESUMEN_TDC_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[2]/div[1]/div[3]/div[1]/sn-product[1]/div[1]/div[1]/span[2]";

	public String C_BTN_PAGA_TARJETA_iOS = "//*[@label='Paga tu tarjeta']";
	public String C_BTN_PAGA_SERVICIOS_iOS = "//*[@label='Paga tus servicios']";
	public String C_BTN_DISPON_EFECTIVO_iOS = "//*[@label='Disp�n de efectivo']";

	public String C_LBL_NO_GENERAR_INTERES_iOS = "//*[@label='Pago para no generar intereses']";
	public String C_LBL_PAGO_MINIMO_iOS = "//*[@label='Pago m�nimo']";
	public String C_LBL_LIMITE_PAGO_iOS = "//*[@label='Fecha l�mite de pago']";

	public String C_DETALLE_TDC = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[5]/div[1]/sn-card[1]/div[1]/div[1]/div[2]";

//	DETALLE DE TDD ANDROID

	public String C_CLICK_TDD = "//*[@id=\'sm_product_checking_accounts_0\']//*[@class=\'sn-card-background sn-card-background__back\']";

//	DETALLE DE TDD

	public String C_CLICK_TDD_iOS = "//*[@id='sm_product_checking_accounts_1']//*[@class='tag-card']";
//	public String C_CLICK_TDD_iOS = "//*[@id=\'sm_product_checking_accounts_0\']//*[@class=\'sn-card-background sn-card-background__back\']";

//ALTA DE NUMERO CELULAR IOS

	public String C_CLICK_TELCEL = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[2]/sn-select-scroll[1]/div[1]/div[2]/div[1]/sn-radio-group[1]/sn-radio-button[1]/div[1]/label[1]/div[3]";

	public String C_MODAL_CONTACTO_iOS = "//*[@class='sn-CHAN25']";

	public String C_CLICK_FILTRAR_TDD_iOS = "//*[@label='Filtrar']";
	public String C_CLICK_RETIROS_TDD_iOS = "//*[@label='Retiros']";
	public String C_CLICK_ANTERIOR_TDD_iOS = "//*[@label='Anterior']";
	public String C_CLICK_TODOS_TDD_iOS = "//*[@label='Todos']";
	public String C_CLICK_DEPOSITOS_TDD_iOS = "//*[@label='Dep�sitos']";
	public String C_CLICK_ACTUAL_TDD_iOS = "//*[@label='Actual']";

	public String C_CLICK_ABONO_TDD_iOS = "//*[@label='15/Ene/20']";
	public String C_CLICK_CERRAR_TDD_iOS = "//*[@label='Cerrar']";
	public String C_CLICK_MOVIMIENTO_DET_TDD_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[3]/div[1]/div[3]/div[1]/div[1]/span[1]";
	public String C_EXITS_DETALLE_MOV_LABEL_iOS = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[1]/h1[1]";

	public String C_CLICK_PRODUCTO_TDD_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[3]/sn-product[1]/div[1]/div[1]/span[3]";
	public String C_CLICK_PRODUCTO_TDD_iOS1 = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[3]/sn-product[1]/div[1]/div[1]/span[3]";
	public String C_ClICK_PRODUCTO_TDD_iOS  ="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/sn-tag[1]/div[1]/div[1]";
	//son dos productos diferentes ;)
//	public String C_LABEL_DETALLECABECERA_TDD_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/sm-products-summary-view[1]/sn-top-bar[1]/header[1]/nav[1]/div[2]";

//	public String C_CLICK_PRODUCTO_TDD_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[2]/sn-product[1]/div[1]/sn-tag[1]/div[1]/div[1]";
//	public String C_CLICK_PRODUCTO_TDD_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/sn-tag[1]/div[1]/div[1]";
	public String C_CLICK_REVERSO_TDD_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[1]/sn-card[1]/div[2]/div[2]/div[2]";

	// public String C_LABEL_DETALLECABECERA_TDD_iOS =
	// "/html[1]/body[1]/sm-root[1]/div[1]/sm-products-summary-view[1]/sn-top-bar[1]/header[1]/nav[1]/div[2]";
	// public String C_LABEL_DETALLECABECERA_TDD_iOS =
	// "/html[1]/body[1]/sm-root[1]/div[1]/sm-products-summary-view[1]/sn-top-bar[1]/header[1]/nav[1]/div[2]";

	
	public String C_LABEL_DETALLECABECERA_TDD_iOS ="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/sn-top-bar[1]/header[1]/nav[1]/div[2]";
	public String C_CLICK_CERRAR_SESION_iOS = "//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeWebView[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[15]/XCUIElementTypeStaticText[1]";
	public String C_BTN_CONFIRMAR_iOS = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog-content[1]/div[1]/div[3]/sm-dialog-close-login[1]/div[1]/button[1]";


   
//	fer ios
	public String C_BTN_MAS_iOS="//*[@class='sn-FUNC056']";
//	public String C_BTN_MAS_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[2]/sn-navbar[1]/footer[1]/div[4]/div[1]";



	public String C_LBL_ULTIMOSMOVIMIENTOS_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[6]/div[3]/div[1]/div[1]/h5[1]";
//	public String C_CLICK_MOVIMIENTO_TDC="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[2]/div[3]/div[1]/div[3]/div[1]/div[1]/span[3]";
	public String C_LBL_DETALLEDEMOVIMIENTO_IOS = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[1]/h1[1]";
	public String C_CLICK_CERRARDMOV_TDC = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[1]/button[1]";

//	public String C_BTN_MAS_iOS ="//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeWebView[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[56]";
//	public String C_CLICK_CERRAR_SESION_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-more-main-menu-view[1]/div[1]/div[2]/sm-button-list[4]/div[1]/div[2]/span[1]";
//	public String C_BTN_CONFIRMAR_iOS = "//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeWebView[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[29]]";
//	public String C_BTN_CONTSER_iOS="//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeWebView[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[9]/XCUIElementTypeOther[2]/XCUIElementTypeStaticText[1]";
	public String C_BTN_CONTSER_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-more-main-menu-view[1]/div[1]/div[2]/sm-button-list[1]/div[1]/div[2]/span[1]";
	public String C_BTN_CONTACTOS1_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/div[1]/sn-tabs[1]/div[1]/div[2]/sn-tab[1]/div[1]/sm-payees-container[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/sn-hidden-buttons[1]/div[1]/div[1]/div[1]/sn-avatar[1]/div[1]/div[2]/div[1]";

	public String C_BTN_CONTACTOS2_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/div[1]/sn-tabs[1]/div[1]/div[2]/sn-tab[1]/div[1]/sm-payees-container[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/sn-hidden-buttons[1]/div[1]/div[1]/div[1]/sn-avatar[1]/div[1]/div[2]/div[1]";
	public String C_LBL_TUSContactos_iOS = "//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeWebView[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[42]/XCUIElementTypeStaticText[1]";
	public String C_BTN_Supermovil_iOS = "//*[@label='supermovilMXExtension']";
	public String C_BTN_Supermovil1_iOS = "//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeCollectionView[1]/XCUIElementTypeCell[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeCollectionView[1]/XCUIElementTypeButton[1]";
	public String C_BOX_SUPERTOKEN_iOS = "//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]";
	public String C_BTN_ACEPTARSUPERTOKEN_iOS = "//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]";
	public String C_BTN_RegresarB_iOS = "//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeWebView[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[1]";

	public String C_CLICK_PASADO_TDD_iOS = "//*[@label='Pasado']";
//	public String C_CLICK_ANTERIOR_TDD_iOS = "//*[@label='Anterior']";
//	public String C_CLICK_ABONO_TDD_iOS = "//*[@label='27/Jun/19']";
	public String C_CLICK_CERRAR_TDD_iOs = "//*[@label='Cerrar']";


	public String C_CLICK_TERCEROSMISMOSBANCOS ="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payments-tdc-others-view[1]/div[2]/div[1]/div[1]/div[4]/sn-tag[1]/div[1]/div[1]";
	public String C_ClICK_TERCEROSMISMOSBANCOS2 ="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payments-tdc-others-view[1]/div[2]/div[1]/div[1]/div[4]/sn-tag[1]/div[1]/div[1]]";
	public String C_CLICK_MONTO="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-select-tdc-others-view[1]/div[1]/div[1]/div[1]/div[2]/form[1]/sn-transfer-field[1]/div[1]/div[2]/div[1]/input[1]";
	public String C_ClICK_BUSCAR_TMB ="(//input[@id=\"searchInput\"])[1]";
	public String C_Label_SINCONTACTOS="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payments-tdc-others-view[1]/div[2]/div[1]/img[1]";
	public String C_BTN_BORRAR ="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payments-tdc-others-view[1]/div[1]/div[1]/sn-search-bar[1]/div[1]/div[2]/sn-icon[1]/i[1]";
	

	public String C_CLICK_FILTRAR_TDC_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[3]/div[3]/div[1]/div[1]/div[1]/sn-chip[1]/div[1]/span[1]/span[1]";
	public String C_CLICK_ACTUAL_TDC_iOS = "";
	public String C_CLICK_PASADO_TDC_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[2]/div[3]/div[1]/div[2]/div[1]/div[2]/sn-chip[2]/div[1]/span[1]/span[1]";
	public String C_CLICK_ANTERIOR_TDC_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[4]/div[3]/div[1]/div[1]/div[1]/div[2]/sn-chip[3]/div[1]/span[1]/span[1]";
	public String C_CLICK_TODOS_TDC_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[2]/div[3]/div[1]/div[2]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]";
	public String C_CLICK_INGRESOS_TDC_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[4]/div[3]/div[1]/div[1]/div[2]/div[2]/sn-chip[2]/div[1]/span[1]/span[1]";
	public String C_CLICK_EGRESOS_TDC_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[4]/div[3]/div[1]/div[1]/div[2]/div[2]/sn-chip[3]/div[1]/span[1]/span[1]";
	public String C_CLICK_PORAPROBAR_TDC_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[3]/div[3]/div[1]/div[2]/div[2]/div[2]/sn-chip[4]/div[1]/span[1]/span[1]";
	public String C_EXITS_DETALLE_MOV_LABEL_TDC_iOS = "";
	public String C_CLICK_CERRAR_TDC_iOS = "(//span[@id=\\\"Cerrar_Modal\\\"])[1]";


	public String C_LABEL_SIN_MOVIMIENTOS="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[2]/div[3]/div[1]/sm-not-filter-deposit[1]/div[1]/h4[1]";

	// Pagar servicios iOS Jair
	public String C_Btn_PagaServiciosMiVida = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-my-life-view[1]/div[1]/sm-info-accounts[1]/div[1]/div[3]/sn-icon-button[3]/div[1]/div[1]/div[1]";
	public String C_Btn_AceptaSuperTokAS = "//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]";


	public String C_Btn_CFE = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-services-view[1]/div[1]/div[1]/div[2]/div[5]/sm-section-service[1]/div[1]/div[1]/div[1]/div[1]";
	public String C_setRefer = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-process[1]/div[1]/form[1]/div[1]/sn-transfer-field[1]/div[1]/div[2]/div[1]/input[1]";
	public String C_setMontoPS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-process[1]/div[1]/form[1]/div[2]/sn-transfer-field[1]/div[1]/div[2]/div[1]/input[1]";
	public String C_Btn_Pagar = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-process[1]/div[1]/div[5]/button[1]";
	public String C_Btn_AceptaSuperTok = "//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]";
	public String C_Btn_CerrarComprob = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-service-voucher[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[3]/div[1]/sn-icon[1]/i[1]";
	
		
		//	Pagar servicio entry point (contactos y servicios) Brayan
	
		public String C_Btn_PagaServicioCyS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-more-main-menu-view[1]/div[1]/div[2]/sm-button-list[1]/div[1]/div[2]/span[1]";
		public String C_Btn_ServiciosCyS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/div[1]/sn-tabs[1]/div[1]/div[1]/div[2]";	
		public String C_Opc_CfeCyS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/div[1]/sn-tabs[1]/div[1]/div[2]/sn-tab[2]/div[1]/sm-payment-services-view[1]/div[1]/div[1]/div[1]/div[5]/sm-section-service[1]/div[1]/div[1]/div[1]/div[1]";
		public String C_Opc_PagarServicioCyS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-service-details[1]/div[1]/div[1]/div[3]/button[1]";
		public String C_referenciaCyS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-process[1]/div[1]/form[1]/div[1]/sn-transfer-field[1]/div[1]/div[2]/div[1]/input[1]";
		public String C_setMontoCyS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-process[1]/div[1]/form[1]/div[2]/sn-transfer-field[1]/div[1]/div[2]/div[1]/input[1]";	
		public String C_Btn_pagarCyS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-process[1]/div[1]/div[5]/button[1]";
		public String C_cerrarComprobanteCyS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-service-voucher[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[3]/div[1]/sn-icon[1]/i[1]";	
		public String C_regresarCyS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-service-details[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]";
		public String C_regresarMenuMasCyS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]";
		
//		Validar modal de pendiente  para que cierre la sesion en Android (Alta de servicio) Brayan 
		
		public String C_Opc_ContactosYserviciosAeP = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-more-main-menu-view[1]/div[1]/div[2]/sm-button-list[1]/div[1]/div[2]/span[1]";
		public String C_Opc_ServiciosAeP = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/div[1]/sn-tabs[1]/div[1]/div[1]/div[2]";
//		axtel                           
		public String C_Opc_ServicioAeP = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/div[1]/sn-tabs[1]/div[1]/div[2]/sn-tab[2]/div[1]/sm-payment-services-view[1]/div[1]/div[1]/div[1]/div[2]/sm-section-service[1]/div[1]/div[1]/div[1]/div[1]";
//		axtel                              
		public String C_Opc_ServicioEnProcesoAeP = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/div[1]/sn-tabs[1]/div[1]/div[2]/sn-tab[2]/div[1]/sm-payment-services-view[1]/div[1]/div[1]/div[2]/div[2]/sm-section-service[1]/div[1]/div[1]/div[1]/div[1]";
		public String C_Btn_ConfirmarAltaAeP = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[3]/div[1]/button[1]";
		public String C_Opc_CerrarComprobanteAltaAeP = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-biller-action-voucher[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[3]/div[1]/sn-icon[1]/i[1]";
		public String C_Opc_CerrarComprobanteEnEsperaAeP = "(//span[@id=\"Cerrar_Modal\"])[1]";
		public String C_Opc_regresarMiVidaAeP = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]";
		
	
	//	Pagar servicio entry point (Menu mas) Brayan
		
		public String C_Btn_PagarServicioMM = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-more-main-menu-view[1]/div[1]/div[1]/div[1]/div[3]/sn-icon-button[1]/div[1]/div[1]/div[1]";
		public String C_CfeMM = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-services-view[1]/div[1]/div[1]/div[2]/div[5]/sm-section-service[1]/div[1]/div[1]/div[1]/div[1]";
		public String C_referenciaMM = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-process[1]/div[1]/form[1]/div[1]/sn-transfer-field[1]/div[1]/div[2]/div[1]/input[1]";
		public String C_setMontoMM = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-process[1]/div[1]/form[1]/div[2]/sn-transfer-field[1]/div[1]/div[2]/div[1]/input[1]";
		public String C_Btn_pagarMM = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-process[1]/div[1]/div[5]/button[1]";
		public String C_cerrarComprobanteMM = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-service-voucher[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[3]/div[1]/sn-icon[1]/i[1]";
		
//		Validar modal de pendiente  para que cierre la sesion en iOS (Alta de servicio) Brayan

		public String C_iOSOpc_ContactosYserviciosAeP = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-more-main-menu-view[1]/div[1]/div[2]/sm-button-list[1]/div[1]/div[2]/span[1]";
		public String C_iOSOpc_ServiciosAeP = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/div[1]/sn-tabs[1]/div[1]/div[1]/div[2]";
//		cea                           
		public String C_iOSOpc_ServicioAeP = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/div[1]/sn-tabs[1]/div[1]/div[2]/sn-tab[2]/div[1]/sm-payment-services-view[1]/div[1]/div[1]/div[1]/div[1]/sm-section-service[1]/div[1]/div[1]/div[1]/div[1]";
//		cea                            
		public String C_iOSOpc_ServicioEnProcesoAeP = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/div[1]/sn-tabs[1]/div[1]/div[2]/sn-tab[2]/div[1]/sm-payment-services-view[1]/div[1]/div[1]/div[2]/div[1]/sm-section-service[1]/div[1]/div[1]/div[1]/div[1]";
		public String C_iOSBtn_ConfirmarAltaAeP = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[3]/div[1]/button[1]";
		public String C_Btn_AceptaSuperTokAeP = "//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]";
		public String C_iOSOpc_CerrarComprobanteAltaAeP = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-biller-action-voucher[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[3]/div[1]/sn-icon[1]/i[1]";
		public String C_iOSOpc_CerrarComprobanteEnEsperaAeP = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[1]/button[1]";
		public String C_iOSOpc_regresarMiVidaAeP = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]";

	
//		pagoTDCTercerosOtrosBancosAndroid (Brayan)
		
		public String C_btn_pagarTarjeta ="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-my-life-view[1]/div[1]/sm-info-accounts[1]/div[1]/div[3]/sn-icon-button[3]/div[1]/div[1]/div[1]";
		public String C_btn_BuscarUnContacto ="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payments-tdc-others-view[1]/div[2]/div[1]/div[1]/div[2]/sn-icon-button[1]/div[1]/div[1]/div[1]";
		public String C_Buscar="(//input[@id=\"searchInput\"])[1]";
		public String C_setBanco ="(//input[@id=\"searchInput\"])[1]";
		public String C_setMontoTDC="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-select-tdc-others-view[1]/div[1]/div[1]/div[1]/div[2]/form[1]/sn-transfer-field[1]/div[1]/div[2]/div[1]/input[1]";
		public String C_btn_pagarTDC="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-select-tdc-others-view[1]/div[1]/div[1]/div[2]/div[1]/button[1]";
		//public String C_Opc_tarjeta="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payments-tdc-others-view[1]/div[2]/div[3]/div[2]/sn-product[2]/div[1]/div[1]/span[3]";
		public String C_Opc_cerrCompr="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-reciept[1]/div[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[3]/div[1]/sn-icon[1]/i[1]";                              
		public String TarjetaBbva="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payments-tdc-others-view[1]/div[2]/div[2]/div[1]/sn-product[1]/div[1]/div[1]/span[3]";
		public String TarjetaInbursa="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payments-tdc-others-view[1]/div[2]/div[2]/div[1]/sn-product[1]/div[1]/div[1]/span[3]"; 

//CONSULTA DE RESUMEN DE TDD ALMA

	public String C_CLICK_TDD_MAS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/div[1]/sn-tabs[1]/div[1]/div[2]/sn-tab[1]/div[1]/sm-payees-container[1]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/sn-tag[1]/div[1]/div[1]";
	public String C_CLICK_TDD_PRO = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-card[1]/div[1]/div[2]/div[2]";
									 
	// PAGO DE TDC PROPIAS ALMA ANDROID

	public String C_CLICK_SIN_DEUDA = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[2]/sm-payment-dialog[1]/div[1]/h2[1]";
	public String C_BTN_ACEPTAR_SIN_DEUDA = "//*[@class='sn-modal-close-text']";
	public String C_BTN_OTRACANTIDAD = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-tdc-view[1]/div[1]/div[1]/div[2]/div[2]/sn-chip[2]/div[1]/span[1]/span[1]";

	// ASOCIACION DE NUMERO CELULAR ANDROID ALMA

	public String C_CLICK_ASOCIAR = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-account-detail-view[1]/div[1]/div[2]/div[4]/div[5]/div[1]/p[1]";
	public String C_CLICK_MODIFICAR = "//*[text()=' Modificar ']";
	public String C_BTN_MODIFICAR = "//*[text()=\' Modificar mi n�mero \']";
	public String C_TXT_ASOCIAR = "//*[text()=' Asociar ']";
	public String C_BTN_NUM_ASOCIADO = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[2]/sm-invalid-operation[1]/div[1]/h2[1]";
	public String C_CLICK_ACEPTAR = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[3]/div[1]/button[1]";
	public String C_EXISTS_NUM_ASOC = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[2]/sm-invalid-operation[1]/div[1]/h2[1]";
	public String C_CLICK_REGRESAR = "//*[@class=\'sn-SMOV004\']";
	public String C_BTN_CONFIRMAR_DATOS = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[2]/sm-relate-confirm-view[1]/div[1]/div[2]/button[1]";
	public String C_BTN_BAJANUMERO = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-relate-phone-view[1]/div[1]/div[4]/div[6]/div[1]/p[1]";
	public String C_TIT_RESUMENTDD = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-account-detail-view[1]/div[1]/div[2]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[2]";
	public String C_COMPROBANTE_ASOCIACION ="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-relate-phone-success-view[1]/div[1]/div[1]/strong[1]/h3[1]";
	public String C_COMPROBANTE_NO_ASOCIACION ="/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[2]/sm-error-dialog[1]/div[1]/p[1]";	
	public String C_CERRAR_COMPROBANTE_ASOCIACION = "//*[@class=\'sn-SMOV011\']";
	public String C_CERRAR_COMPROBANTE_NO_ASOCIACION = "//*[@id='Cerrar_Modal']";
	public String C_CLICK_TERMINOSYCONDICIONES = "//*[@class='sn-checkbox-frame']";
	public String C_CLICK_CONFIRMARDATOS = "//*[text()=' Confirmar datos ']";
	public String C_CERRAR_COMPROBANTE_BAJA = "//*[@class=\'sn-SMOV011\']";
	
	//ASOCIACION DE NUMERO CELULAR iOS 

	public String C_CLICK_ASOCIAR_iOS = "//*[label()=\' Asociar \']";
	public String C_CLICK_MODIFICAR_iOS = "//*[label()=\' Modificar \']";
//		public String C_BTN_MODIFICAR = "//*[text()=\' Modificar mi n�mero \']";
//		public String C_TXT_ASOCIAR = "//*[text()=\' Asociar \']"; 
	public String C_EXISTS_NUM_ASOC_iOS = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[2]/sm-invalid-operation[1]/div[1]/h2[1]";
	public String C_CLICK_ACEPTAR_iOS = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[3]/div[1]/button[1]";
//		public String C_EXISTS_NUM_ASOC = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[2]/sm-invalid-operation[1]/div[1]/h2[1]";
	public String C_CLICK_REGRESAR_iOS = "//*[@class=\'sn-SMOV004\']";
//		public String C_CLICK_CANCELARSUPERTOKEN = "//*[@resource-id=\'mx.bancosantander.supermovil:id/btnCancel\']";
	public String C_BTN_CONFIRMAR_DATOS_iOS = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[2]/sm-relate-confirm-view[1]/div[1]/div[2]/button[1]";
//		public String C_BTN_BAJANUMERO = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-relate-phone-view[1]/div[1]/div[4]/div[6]/div[1]/p[1]";
//		public String C_BTN_TERMINOSYCONDICIONES = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[2]/sm-relate-confirm-view[1]/div[1]/div[1]/div[3]/form[1]";


	public String C_Btn_ContactosYServicios = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-more-main-menu-view[1]/div[1]/div[2]/sm-button-list[1]/div[1]/div[2]/span[1]";
	public String C_Btn_Servicios = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/div[1]/sn-tabs[1]/div[1]/div[1]/div[2]";
	public String C_Btn_CFE_contYserv = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/div[1]/sn-tabs[1]/div[1]/div[2]/sn-tab[2]/div[1]/sm-payment-services-view[1]/div[1]/div[1]/div[1]/div[5]/sm-section-service[1]/div[1]/div[1]/div[1]/div[1]";
	public String C_Btn_pagar_serv = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-service-details[1]/div[1]/div[1]/div[3]/button[1]";
	public String C_cerrarCompContaYServi = "";
	
	public String C_FlechaAtras = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-services-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]";
	
	public String C_Btn_PagaServiciosMenuMas = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-more-main-menu-view[1]/div[1]/div[1]/div[1]/div[3]/sn-icon-button[1]/div[1]/div[2]";
	
	public String servicio;
	public String LblServNoDadoDeAlta = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[2]/sm-activate-service[1]/div[1]/p[1]";
	public String nombreServicio = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-process[1]/div[1]/div[3]/sm-service-avatar[1]/div[1]/div[1]/div[1]/p[1]";
	

	// Pago TDC propias iOS Jair
	public String C_Btn_PagarTar = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[2]/div[2]/div[1]/sm-credit-card-slider-buttons[1]/div[1]/sn-icon-button[1]/div[1]/div[1]/div[1]";
	public String C_TarjetaAPagar = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[2]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]";
	public String C_Btn_TipoDePago = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-tdc-view[1]/div[1]/div[1]/div[2]/div[3]/sn-card-select[1]/sn-card-payment[2]/div[1]/div[1]/div[1]/sn-radio-button[1]/div[1]/label[1]/div[3]";
	public String C_LblTarjetaSalPosit = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[2]/sm-payment-dialog[1]/div[1]/h2[1]";
	public String C_CerrarModal = "(//span[@id=\"Cerrar_Modal\"])[1]";
	public String C_setMontoPTDCP = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-tdc-view[1]/div[1]/div[1]/div[2]/form[1]/sn-transfer-field[1]/div[1]/div[2]/div[1]/input[1]";
	public String C_CerrarComprobPTDCP = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-voucher-view[1]/div[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[3]/div[1]/sn-icon[1]/i[1]";
	public String C_swipeLateral = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payment-tdc-view[1]/div[1]/div[2]/sn-slide-button[1]/div[1]/div[1]/div[1]/div[1]/sn-icon[1]/i[1]";
	public String C_btn_PagarTarjetaMiVida = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-my-life-view[1]/div[1]/sm-info-accounts[1]/div[1]/div[3]/sn-icon-button[4]/div[1]/div[1]/div[1]";

	public String Lbl_encabezadoProductos = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[1]/div[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[2]";
	public String Lbl_encabezadoPagarTarjetas = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payments-tdc-others-view[1]/div[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[2]";
	public String C_TarjetaAPagar2 = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payments-tdc-others-view[1]/div[2]/div[1]/div[2]/sn-product[2]/div[1]/div[1]/span[2]";

	public String clickAtras = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-payments-tdc-others-view[1]/div[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]";

	public String C_btn_PagoTDC_Menu_Mas = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-more-main-menu-view[1]/div[1]/div[1]/div[1]/div[4]/sn-icon-button[1]/div[1]/div[1]/div[1]";

	// Dinero Creciente Retiro IOS Jair
	public String C_DineroCreciente_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[3]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[3]";
	public String C_BTN_DCRetiro_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-growing-account-view[1]/div[1]/div[2]/sn-tabs[1]/div[1]/div[1]/div[2]";
	public String C_setMonto = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-growing-account-view[1]/div[1]/div[4]/form[1]/sn-transfer-field[1]/div[1]/div[2]/div[1]/input[1]";
	public String C_BTN_ContinuarDC_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-growing-account-view[1]/div[1]/div[6]/button[1]";
	public String C_Btn_CerrarComp = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-growing-vaucher-view[1]/div[1]/div[1]/div[1]/button[1]/div[2]";
									  
	public String C_SET_Montos_IOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-growing-account-view[1]/div[1]/div[4]/form[1]/sn-transfer-field[1]/div[1]/div[2]/div[1]/input[1]";
	public String C_LBL_Desdetucuenta_IOS = "//*[@value=\"Desde tu cuenta\"]";
	public String C_BTN_Continuar_iOS = "//*[@label=\"Continuar\"]";

//		Alta beneficiario transfer FER

	public String C_CLICK_Bank_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiary-info-view[1]/div[1]/div[3]/form[1]/div[1]/div[3]/div[2]/sn-icon[1]/i[1]";
	public String C_CLICK_Bancos_iOS = "/html[1]/body[1]/div[1]/div[2]/div[1]/sn-dialog[1]/div[1]/div[2]/sm-dialog-list-dialog-view[1]/div[1]/div[2]/div[1]/div[1]/div[1]/sn-radio-group[1]/sn-radio-button[2]/div[1]/label[1]/div[3]";
	public String C_CLICK_Nombre_iOS = "//*[@id=\"name\"]";
	public String C_CLICK_Alias_iOS = "//*[@id=\"alias\"]";
	public String C_CLICK_Agregarcontacto_iOS = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiary-info-view[1]/div[1]/div[4]/button[1]";

	// Disposicion de efectivo Jair iOS
	public String C_Click_Dispo_Efectivo = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[12]/div[2]/div[1]/sm-credit-card-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]";
	public String C_setMontoDispoEfectivo = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-credit-card-money[1]/div[1]/form[1]/div[1]/sn-transfer-field[1]/div[1]/div[2]/div[1]/input[1]";
	public String C_disponer = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-credit-card-money[1]/div[1]/div[3]/button[1]";
	
	public String C_comprobDispEfe = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-credit-card-voucher[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[3]/div[1]/sn-icon[1]/i[1]";
	
	
	//Filtros TDD iOS 
	public String C_LABEL_SIN_MOVIMIENTOSTDD="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[3]/div[1]/sm-not-filter-deposit[1]/div[1]/h4[1]";
//		Modificacion numero celular menu Mas
		
		
	    public String C_CLICK_TarjetaTDD = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/div[1]/sn-tabs[1]/div[1]/div[2]/sn-tab[1]/div[1]/sm-payees-container[1]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[3]";
	    public String C_CLICK_celularnuevo = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-relate-phone-view[1]/div[1]/div[4]/form[1]/div[1]/sn-form-field[1]/div[1]/div[2]";
		public String C_CLICK_confirmarcel = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-relate-phone-view[1]/div[1]/div[4]/form[1]/div[2]/sn-form-field[1]/div[1]/div[2]";
		public String C_CLICK_Regre = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-beneficiaries-services-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]";

		
//		<!-- 		Fer boton ingresos finanzas ios -->
		
		public String C_CLICK_FIngresos = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-my-life-view[1]/div[1]/sm-my-finances-view[1]/div[1]/div[3]/div[1]/sn-chip[2]/div[1]/span[1]/span[1]";
//		Transferencias
		public String C_TRANS_MI_VIDA = "//*[@class=\\\"card-buttons px-4 pb-4 ng-star-inserted\\\"]//*[@class=\\\"ng-star-inserted\\\"]/div[1]/div[1]/div[1]";
		
		
		
		//ELEMENTS CLARIFICATIONS	 
		public String RESUMEN_TDD ="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-summary-view[1]/div[3]/div[1]/div[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]";
		public String BTC_CLARIFY_MOVEMENT="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[2]/div[1]/sm-cheking-accounts-slider-buttons[1]/div[1]/sn-icon-button[3]/div[1]/div[1]/div[1]";
		
		public String  LABEL_FRAUDE= "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]";
		public String  NATIONAL= "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]";
		public String  FOREIGN= "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/div[2]/sn-chip[2]/div[1]/span[1]/span[1]";
		public String  STATE= "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[3]/div[1]/div[1]/select[1]/option[33]";		            	
		public String  BTNCONTINUAR= "(//button[@id=\"aceptar-button\"])[1]";
		public String  MOVEMENT1= "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]";
		public String  MOVEMENT2= "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[2]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]";
		public String  MOVEMENT1TDC= "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[3]/ul[1]/li[1]/sm-movement-component[1]/div[2]/span[1]";
		public String  MOVEMENT2TDC= "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[3]/ul[2]/li[1]/sm-movement-component[1]/div[2]/span[1]";
		public String  BTNCONTINUAR2= "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/footer[1]/div[1]/button[1]";
		public String  INFOBANCARIA="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/div[2]/span[1]";
		public String  CLOSERECEIPT= "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[1]/div[1]/sn-icon[1]/i[1]";
		
		//Elements para Robo
		public String btnStolen = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[2]/div[1]/span[1]";
		public String btnMexico = "/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[3]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]";
		public String btnForeign ="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[3]/div[1]/div[2]/div[2]/sn-chip[2]/div[1]/span[1]/span[1]";
				
	    //Elements 7 motivos
		
		public String LBL_WHAT_IS_THE_SITUATION="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[2]";
		public String LBL_CLARI_PURCHASE_OR_SERVICE="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/sm-blue-header-component[1]/div[1]/h2[1]";
		public String LBL_MOVEMENTS_IN_SHOPS="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/sm-blue-header-component[1]/div[1]/span[1]";
		public String LBL_FRAUD="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[1]/div[1]/span[1]";
		public String LBL_STOLE="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[2]/div[1]/span[1]";
		
		public String LBL_DOUBLE_CHARGE="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[3]/div[1]/span[1]";
		public String LBL_INCORRECT_AMOUNT="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[4]/div[1]/span[1]";
		public String LBL_UNAUTHORIZED_PAYMENT="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[5]/div[1]/span[1]";
		public String LBL_CASH_PAYMENT="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[6]/div[1]/span[1]";
		public String LBL_PURCHASE_OR_SERVICE="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[7]/div[1]/span[1]";
		public String LBL_SERVICE_CANCELED="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[8]/div[1]/span[1]";
		public String LBL_NO_REFUND="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-questionary-view[1]/div[1]/div[1]/div[9]/div[1]/span[1]";
		public String BOX_WHAT_HAPPENED="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[2]/textarea[1]";
		
		
		public String LBL_QUESTIONNAIRE="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[2]";
		public String LBL_SCREEN2_SEVEN_REASONS="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[1]/h2[1]";
		public String LBL_WHAT_HAPPENED="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[1]/div[1]/div[1]/span[1]";
		public String LBL_CURRENT_PLACE="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[1]/span[1]";
		public String LBL_WHERE_ARE_YOU="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[2]/div[1]/span[1]";
		public String BTN_SCREEN2_NATIONAL="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[2]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]";
		public String BTN_SCREEN2_FOREIGN="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[2]/div[1]/div[2]/div[2]/sn-chip[2]/div[1]/span[1]/span[1]";
		public String LBL_FEDERAL_ENTITY="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-entity-view[1]/div[1]/div[2]/form[1]/section[3]/div[1]/div[1]/select[1]/option[33]";
		
		public String LBL_SELECTION_MOVEMENTS="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[2]";
		public String LBL_CARD_NAME="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[1]/sn-product[1]/div[1]/div[1]/span[2]";
		public String BTN_FILTRAR="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/sm-filter-component[1]/div[1]/div[2]/sn-chip[1]/div[1]/span[1]/span[1]";
		public String LBL_SELECT_MOVEMENTS="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/div[1]/h2[1]";
		public String LBL_MOVEMENTS_TO_CLARIFY="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/div[2]/span[1]";
		public String LBL_MXN="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[1]/div[1]/span[3]";
		public String LBL_MOVEMENT_NAME="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-movements-view[1]/div[1]/div[2]/sm-list-component[1]/div[1]/ul[1]/li[1]/sm-movement-component[1]/div[1]/div[2]/span[1]/span[6]/span[1]";
		public String LBL_DATE_MOVEMENT="(//span[@id=\"date-4471\"])[1]";
		
		//comprobante
		
		public String LBL_RECEIPT="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/sn-top-bar[1]/header[1]/nav[1]/div[2]";
		public String LBL_CLARIFICATIONS_SENT="(//h2[@id=\"tittle-0\"])[1]";
		public String LBL_FAST_ANSWER="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/div[1]/div[1]/div[1]/h3[1]/h4[1]/span[1]/span[1]";
		public String LBL_FOLIO="(//h3[@id=\"folioNal-0\"])[1]";
		public String LBL_SHOW_INFO_BANCARIA="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/div[2]/span[1]";
		public String BTN_SHARE_RECEIPT="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/div[4]/div[1]/div[1]/sn-icon-button[1]/div[1]/div[2]";
		public String BTN_SAVE_IMAGE="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/div[4]/div[1]/div[2]/sn-icon-button[1]/div[1]/div[2]";
		public String LBL_DATE_AND_TIME="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/h4[1]/span[1]";
		public String LBL_DATE_AND_TIME_VALUE="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/h4[1]/span[1]";
		public String LBL_COMMITMENT_DATE="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/h4[1]/span[1]";
		public String LBL_COMMITMENT_DATE_VALUE="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/div[1]/div[1]/div[1]/div[4]/div[2]/h4[1]/span[1]";
		public String LBL_CLARIFICATIONS_AMOUNT="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/div[1]/div[1]/div[1]/div[5]/div[1]/h4[1]/span[1]";
		public String LBL_CLARIFICATIONS_AMOUNT_VALUE="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/div[1]/div[1]/div[1]/div[5]/div[2]/h4[1]/span[1]";
		public String LBL_HIDE_INFO_BANCARIA="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-result-view[1]/div[1]/div[2]/span[1]";
		
			
		public static final String COLOR_000000= "#000000";
		public static final String COLOR_f2f2f2= "#f2f2f2";
		public static final String COLOR_ffffff= "#ffffff";
		public static final String COLOR_7676767= "#767676";
		public static final String COLOR_ec0000= "#ec0000";
		public static final String COLOR_ec00= "#ec00";
		
		
		public static final String RGBA_0_0_0_087= "rgba(0, 0, 0, 0.87)";
		
		public static final String SANTANDER_HEAD_LINE  = "SantanderHeadline";
		public static final String SANTANDER_TEXT  = "SantanderText";
		public static final String OPENSANS  = "OpenSans";
				
				
//		TEXT primera pantalla aclaraciones
		
		public String LBL_WHAT_IS_THE_SITUATION_TEXT="�Cu�l es tu situaci�n?";
		public String LBL_CLARI_PURCHASE_OR_SERVICE_TEXT="Aclaraciones por compras en comercios";
		public String LBL_MOVEMENTS_IN_SHOPS_TEXT="Los movimientos que por ahora se pueden aclarar \r\n" + 
				"solo son consumos en comercios (compras).";
		public String LBL_FRAUD_TEXT="Tengo un cobro que yo no hice";
		public String LBL_STOLE_TEXT="Me robaron o extravi� mi tarjeta";
		
		public String LBL_DOUBLE_CHARGE_TEXT="Me hicieron un doble cobro";
		public String LBL_INCORRECT_AMOUNT_TEXT="La cantidad que me cobraron es incorrecta";
		public String LBL_UNAUTHORIZED_PAYMENT_TEXT="Me cobraron cosas que no autoric�";
		public String LBL_CASH_PAYMENT_TEXT="Pagu� en efectivo u otra tarjeta";
		public String LBL_PURCHASE_OR_SERVICE_TEXT="No recib� la compra o servicio";
		public String LBL_SERVICE_CANCELED_TEXT="Me cobraron un servicio que cancel�";
		public String LBL_NO_REFUND_TEXT="Regres� lo que compr� y no recib� la devoluci�n";
		
//		TEXT SEGUNDA  pantalla aclaraciones
		
		public String LBL_QUESTIONNAIRE_TEXT="Cuestionario";
		
		public String LBL_SCREEN2_DOUBLE_CHARGE_TEXT="Me hicieron un doble cobro";
		public String LBL_SCREEN2_INCORRECT_AMOUNT_TEXT="La cantidad que me cobraron es incorrecta";
		public String LBL_SCREEN2_UNAUTHORIZED_PAYMENT_TEXT="Me cobraron cosas que no autoric�";
		public String LBL_SCREEN2_CASH_PAYMENT_TEXT="Pagu� en efectivo u otra tarjeta";
		public String LBL_SCREEN2_PURCHASE_OR_SERVICE_TEXT="No recib� la compra o servicio";
		public String LBL_SCREEN2_SERVICE_CANCELED_TEXT="Me cobrar�n un servicio que cancele";
		public String LBL_SCREEN2_NO_REFUND_TEXT="Regres� lo que compr� y no recib� la devoluci�n";
		
		public String LBL_WHAT_HAPPENED_TEXT="Describe lo que sucedi�:";
		public String LBL_CURRENT_PLACE_TEXT="Para continuar es necesario saber desde d�nde realizas tu aclaraci�n:";
		public String LBL_WHERE_ARE_YOU_TEXT="�D�nde est�s?";
		public String BTN_SCREEN2_NATIONAL_TEXT="M�xico";
		public String BTN_SCREEN2_FOREIGN_TEXT="Extranjero";
		public String LBL_FEDERAL_ENTITY_TEXT="Entidad federativa";
		public String LBL_CONTINUAR_TEXT="Continuar";
		
		
//		TEXT tercera  pantalla aclaraciones
		
		public String LBL_SELECTION_MOVEMENTS_TEXT="Selecci�n de movimientos";
		public String LBL_CARD_NAME_TEXT="Cuenta free";
		public String BTN_FILTRAR_TEXT="Filtrar";
		public String LBL_SELECT_MOVEMENTS_TEXT="Seleccionar movimientos";
		public String LBL_MOVEMENTS_TO_CLARIFY_TEXT="Elige uno o m�s movimientos que quieras aclarar:";
		public String LBL_MXN_TEXT="MXN";
		public String LBL_MOVEMENT_NAME_TEXT="NOMBBRE DEL MOVIMIENTO";
		public String LBL_DATE_MOVEMENT_TEXT="fecha";
		public String BTN_ONE_MOVEMENT_TEXT="Aclarar (1) movimiento(s)";
		public String BTN_TWO_MOVEMENT_TEXT="Aclarar (2) movimientos";
		
//		TEXT elementos del comprobante
		
		
		public String LBL_RECEIPT_TEXT="Comprobante";
		public String LBL_CLARIFICATIONS_SENT_TEXT="Aclaraci�n enviada";
		public String LBL_FAST_ANSWER_TEXT="Investigaremos tu caso para darte una pronta respuesta";
		public String LBL_FOLIO_TEXT="Folio";
		public String LBL_SHOW_INFO_BANCARIA_TEXT="Mostrar informaci�n bancaria";
		public String BTN_SHARE_RECEIPT_TEXT="Compartir comprobante";
		public String BTN_SAVE_IMAGE_TEXT="Guardar en imagen";
		public String LBL_DATE_AND_TIME_TEXT="Fecha y hora";
		public String LBL_DATE_AND_TIME_VALUE_TEXT="25/Feb/2019 23:11";
		public String LBL_COMMITMENT_DATE_TEXT="Fecha compromiso";
		public String LBL_COMMITMENT_DATE_VALUE_TEXT="31/May/2019";
		public String LBL_CLARIFICATIONS_AMOUNT_TEXT="Monto de la aclaraci�n";
		public String LBL_CLARIFICATIONS_AMOUNT_VALUE_TEXT="$ 1,400.00";
		public String LBL_HIDE_INFO_BANCARIA_TEXT="Ocultar informaci�n bancaria";
		
		
		
		
		
		public String LBL_ULTIMOS_MOVIMIENTOS="/html[1]/body[1]/sm-root[1]/div[1]/div[1]/sm-products-summary-view[1]/div[3]/div[1]/div[3]/div[1]/div[1]/h5[1]";
		
		
		
		
		
		
		
		
}
