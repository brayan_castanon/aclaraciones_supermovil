package mx.com.santander.suite;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

import mx.com.santander.suite.test.Prueba;
//import mx.com.santander.suite.test.Pruebas;

public class Proyecto
{	public static void main	( String[] args )
    {	TestListenerAdapter tla = new TestListenerAdapter();
        TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] {Prueba.class});
		testng.addListener(tla);
		testng.run();
		System.exit(0);
    }
	
	
}
