package mx.com.santander.suite.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.xframium.Initializable;
import org.xframium.device.factory.DeviceWebDriver;
import org.xframium.device.ng.AbstractJavaTest;
import org.xframium.device.ng.TestContainer;
import org.xframium.device.ng.TestName;
import org.xframium.device.ng.TestPackage;
import org.xframium.driver.ConfigurationReader;
import org.xframium.driver.TXTConfigurationReader;
import org.xframium.driver.XMLConfigurationReader;
import org.xframium.page.StepStatus;
import org.xframium.page.keyWord.KeyWordPage;
import mx.com.santander.Evidencias;
import java.util.Properties;
import java.io.FileReader;


import java.util.List;
import org.testng.Assert;
import org.xframium.page.data.PageData;
import org.xframium.page.data.PageDataManager;
import org.xframium.page.data.provider.PageDataProvider;
import mx.com.santander.suite.test.pageobject.HomeFunciones;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import java.util.concurrent.TimeUnit;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.xframium.container.SuiteContainer;
import org.xframium.reporting.ExecutionContextTest;



public class Pruebas extends AbstractJavaTest

{	
	
	private static ConfigurationReader configurationReader = null;
	public static String pathXmlConfig;
	public static String xFIDPrueba;
	private HomeFunciones homeFunciones = null;
	public static PageData[] SearchData1=null;
	public static String varRecordType=null;
	public static Integer varIndice=0; 
	public static Integer varIndiceAux=0;
	public static PageDataProvider pageDataProvider =null;
//	private Home home = null;
	
	
	
    @AfterSuite
    public void initialize() throws FileNotFoundException, IOException
    {	
   
   
    	// It's faster when auto commit is off 
   // conn.setAutoCommit (false); 
   
	Properties p = new Properties();

	System.out.println("Entra a prueba java");
if(System.getProperty("os.name").toLowerCase().contains("win"))
	{ 

	p.load(new FileReader("resources\\properties\\configTest.properties"));
	pathXmlConfig=p.getProperty("pathTest");
	System.out.println("Entra a windows" + pathXmlConfig);
        if(p.getProperty("pathTest").contains("$"))
		{ 
               
               pathXmlConfig=p.getProperty("pathTest").replace("/","\\");
			   System.out.println(pathXmlConfig);
	        }
	}
else
	{ 
	
	p.load(new FileReader("resources/properties/configTest.properties"));
	pathXmlConfig=p.getProperty("pathTest");
       if(p.getProperty("pathTest").contains("\\"))
		{ 
    	   pathXmlConfig=p.getProperty("pathTest").replace("\\","/");
		   
	        }

}
System.out.println("Asignar variable: " + pathXmlConfig);		
File configurationFile = new File(pathXmlConfig);
System.out.println("Comprobando variable: " + pathXmlConfig);
		Initializable.xFID.set(UUID.randomUUID().toString() ); 
		xFIDPrueba=Initializable.xFID.get();
		Map<String,String> customConfig = new HashMap<String,String>(5);
        customConfig.put("xF-ID", Initializable.xFID.get());
        
        if(configurationFile.getName().toLowerCase().endsWith(".xml"))
        {    configurationReader = new XMLConfigurationReader();
        }else 
		{	if(configurationFile.getName().toLowerCase().endsWith(".txt"))
			{	configurationReader = new TXTConfigurationReader();
			}
		}
        
        configurationReader.readConfiguration(configurationFile, true, customConfig);
       
        
        //**Se manda llamar la clase "Evidencias" para generar el reporte con el formato 502**//
       
        executeCommand("java -jar evidencias502.jar resources/properties/configTest.properties"); 
    
    
     
      
       
      // Evidencias evidencias = new Evidencias();
    //   try {          
      //                  Evidencias.ejecutar();
                        
        //} catch (Exception e)
        //{              e.printStackTrace();
        //} 
    }
    
    
    private static String executeCommand(String command) 
    
    { StringBuffer output = new StringBuffer();
    Process p;
    System.out.println("Generaci�n de Evidencia 502 "+command);
    try 
    { p = Runtime.getRuntime().exec(command);
    p.waitFor();
    BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

                String line = "";
    while ((line = reader.readLine())!= null) 
    { output.append(line + "\n");
    }
    }catch(Exception e) 
    { e.printStackTrace();
    }
    return output.toString();
    }
    
	
	
	 @When( "^clic ingresar$")
    public void ClickIngresar( WebDriver webDriver )
    {
    	
    	 try
         {   
    		
    		
         homeFunciones = (HomeFunciones) createPage(HomeFunciones.class, (DeviceWebDriver) webDriver);
         homeFunciones.clicBotonIngresar();
     
         //  homeFunciones.clicBotonIngresar("//XCUIElementTypeButton[@name='btnAcceso']"); IOS
       
         }
         catch( Exception e )
         {   e.printStackTrace();
         }
       
    }
    
      
    
    
    @When( "^clic ambiente$")
    public void ClickAmbiente( WebDriver webDriver )
    {
    	
		
    	 try
         {   
    		
    		
         homeFunciones = (HomeFunciones) createPage(HomeFunciones.class, (DeviceWebDriver) webDriver);
        
         homeFunciones.clicTipoAmbiente("//XCUIElementTypeApplication[@name='Santander']/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[4]/XCUIElementTypeButton");
         
        
         }
         catch( Exception e )
         {   e.printStackTrace();
         }
       
    }
    
    
  
  
  
}
