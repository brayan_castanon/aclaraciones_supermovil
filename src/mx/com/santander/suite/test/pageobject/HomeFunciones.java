package mx.com.santander.suite.test.pageobject;

import org.xframium.device.factory.DeviceWebDriver;
import org.xframium.page.Page;
import org.xframium.page.Page.ElementDefinition;

/**
 * Definitions for page Home.
 * 
 * 
 */
public interface HomeFunciones extends Page
{	
 
    public void crearMeta(String Meta, String Saldo, String xpathMeta, String xpathSaldo) throws Exception;
	public void clicBotonIngresar() throws Exception;
	public void clicTipoAmbiente(String id) throws Exception;


}
