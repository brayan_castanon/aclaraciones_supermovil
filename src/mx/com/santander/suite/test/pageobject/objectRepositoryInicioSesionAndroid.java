package mx.com.santander.suite.test.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.xframium.device.factory.DeviceWebDriver;
import org.xframium.page.Page;
import org.xframium.page.Page.ElementDefinition;

public interface objectRepositoryInicioSesionAndroid extends Page

{	
	@ElementDefinition
	public static final String INGRESAR_1 = "ingresar";
	@ElementDefinition
	public static final String CODCLIENTE = "boxCodCliente";
	@ElementDefinition
	public static final String BTNCONTINUAR = "btnContinuar";
	@ElementDefinition
	public static final String PASSWORD = "boxPassword";
	@ElementDefinition
	public static final String BTNCONTINUAR2 = "btnContinuar2";
	@ElementDefinition
	public static final String LEYENDA = "misPruductos";
	@ElementDefinition
	public static final String BIENVENIDO = "bienvenido";
	@ElementDefinition
	public static final String SANTANDERMOVIL = "santanderMovil";
	@ElementDefinition
	public static final String ACCESO = "acceso";
	@ElementDefinition
	public static final String DATOS_ID = "datos_identificarte";
	@ElementDefinition
	public static final String MSJINGPASS = "msjIngresaPass";
	@ElementDefinition
	public static final String PASSUSUARIO = "PassUsuario";
	@ElementDefinition
	public static final String MIVIDA = "miVida";
	@ElementDefinition
	public static final String BTNMISPRODUCTOS = "btnProductos";
	@ElementDefinition
	public static final String MODLIGTH = "modoLigth";
	@ElementDefinition
	public static final String CONFIRMACIONMOD = "confirmacionMod";
	
	public void clickIngresar() throws Exception; 
	public void setIngresarNumCliente(String usuario) throws Exception;
	public void clickContinuar() throws Exception; 
	public void setPassword(String password) throws Exception; 
	public void clickContinuar2() throws Exception; 
	public void existMiVida() throws Exception;
	public void clickLight() throws Exception; 
	public void clickConfirmacionMod() throws Exception;

}



	
	
	
