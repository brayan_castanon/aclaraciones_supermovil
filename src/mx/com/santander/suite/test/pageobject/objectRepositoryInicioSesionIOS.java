package mx.com.santander.suite.test.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import io.appium.java_client.android.AndroidDriver;

import org.xframium.Initializable;
import org.xframium.device.factory.DeviceWebDriver;
import org.xframium.page.Page;
import org.xframium.page.Page.ElementDefinition;

public interface objectRepositoryInicioSesionIOS extends Page

{	
	
	@ElementDefinition
	public static final String IMG_INICIO = "imgInicio";
	@ElementDefinition
	public static final String MSJWELCOM = "msjBienvenida";
	@ElementDefinition
	public static final String INGRESAR_1 = "ingresar";
	@ElementDefinition
	public static final String MSJ_ACCESO = "msjAcceso";
	@ElementDefinition
	public static final String MSJ_DATOS = "msjIngresaDatos";
	@ElementDefinition
	public static final String CODCLIENTE = "boxCodCliente";
	@ElementDefinition
	public static final String BTNCONTINUAR = "btnContinuar";
	@ElementDefinition
	public static final String MSJ_PASS = "msjIngresaPass";
	@ElementDefinition
	public static final String MSJ_PCR = "msjPCR";
	@ElementDefinition
	public static final String PASSWORD = "boxPassword";
	@ElementDefinition
	public static final String BTNCONTINUAR2 = "btnContinuar2";
	@ElementDefinition
	public static final String PRODUCTOS = "productos";
	@ElementDefinition
	public static final String LEYENDA = "misPruductos";
	@ElementDefinition
	public static final String BTNDONE = "btnDone";
	@ElementDefinition
	public static final String BTNDONE1 = "btnDone1";
	@ElementDefinition
	public static final String MIVIDA = "miVida";
	@ElementDefinition
	public static final String BTNOK = "btnOK";
	
	
	


	public void clickIngresar() throws Exception; 
	public void setIngresarNumCliente(String usuario) throws Exception;
	public void clickContinuar() throws Exception; 
	public void setPassword(String password) throws Exception; 
	public void clickContinuar2() throws Exception; 
	public void clickProductos() throws Exception; 
	public void borrar() throws Exception; 

}



	
	
	




	
	
	
	
	
