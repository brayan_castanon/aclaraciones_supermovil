package mx.com.santander.suite.test.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import io.appium.java_client.android.AndroidDriver;

import org.xframium.Initializable;
import org.xframium.device.factory.DeviceWebDriver;
import org.xframium.page.Page;
import org.xframium.page.Page.ElementDefinition;

public interface objectRepositorySevenReasonsTDDAndroid extends Page

{	

	
	
	@ElementDefinition
	public static final String CLICKTARJETATDD = "clickTarjetaTDD";
	@ElementDefinition
	public static final String CLICKBTNACLARARMOVIMIENTO = "clickBtnAclararMovimiento";
	
	
//	Aclaraciones project
	
	public void clickTarjetaTDD() throws Exception;
	public void clickBtnClarifiMovement() throws Exception;
	public void LabelDoubleCharge() throws Exception;
	public void LabelIncorrectAmount() throws Exception;
	public void LabelUnauthorizedPayment() throws Exception;
	public void LabelCashPayment() throws Exception;
	public void LabelPurchaseOrService() throws Exception;
	public void LabelServiceCanceled() throws Exception;
	public void LabelNoRefund() throws Exception;
	public void BoxWhatHappened() throws Exception;
	public void National() throws Exception;
	public void Foreign() throws Exception;
	public void State() throws Exception;
	public void BtnContinuar() throws Exception;
	public void Movement1() throws Exception;
	public void Movement2() throws Exception;
	public void Movement1TDC() throws Exception;
	public void Movement2TDC() throws Exception;
	public void BtnContinuar2() throws Exception;
	public void LblInfoBancaria() throws Exception;
	public void CloseReceipt() throws Exception;
	
	public void ScreenWhatIsTheSituation() throws Exception;
	public void LblWhatIsTheSituation() throws Exception;
	public void MeHicieronUnCargoDoble() throws Exception;
	
	//primera pantalla validaciones
	
	public void Vali_lbl_WhatIsTheSituation() throws Exception;
	public void Vali_lbl_Clari_PurchaseOrService() throws Exception;
	public void Vali_lbl_MovementsInShops() throws Exception;
	public void Vali_lbl_Fraud() throws Exception;
	public void Vali_lbl_Stole() throws Exception;
	public void Vali_lbl_DoubleCharge() throws Exception;
	public void Vali_lbl_IncorrectAmount() throws Exception;
	public void Vali_lbl_UnauthorizedPayment() throws Exception;
	public void Vali_lbl_CashPayment() throws Exception;
	public void Vali_lbl_PurchaseOrService() throws Exception;
	public void Vali_lbl_ServiceCanceled() throws Exception;
	public void Vali_lbl_NoRefund() throws Exception;
	
	
	//segunda pantalla validaciones
	
	public void Vali_lbl_Questionnaire() throws Exception;
	
	public void Vali_lbl_Double_Charge() throws Exception;
	public void Vali_lbl_Incorrect_Amount() throws Exception;
	public void Vali_lbl_Unauthorized_Payment() throws Exception;
	public void Vali_lbl_Cash_Payment() throws Exception;
	public void Vali_lbl_Purchase_Or_Service() throws Exception;
	public void Vali_lbl_Service_Canceled() throws Exception;
	public void Vali_lbl_Label_NoRefund() throws Exception;
	
	
	
	
	
	
	
	public void Vali_lbl_What_Happened() throws Exception;
	
	public void Vali_lbl_Current_Place() throws Exception;
	public void Vali_lbl_Where_Are_You() throws Exception;
	public void Vali_Btn_National() throws Exception;
	public void Vali_Btn_Foreign() throws Exception;
	public void Vali_Lbl_Federal_Entity() throws Exception;
	public void Vali_Btn_Continuar() throws Exception;
	
	
	//tercera pantalla validaciones
	
	
	public void Vali_Lbl_Selection_Movements() throws Exception;
	public void Vali_Lbl_CardName() throws Exception;
	public void Vali_Btn_Filtrar() throws Exception;
	public void Vali_Lbl_Select_Movements() throws Exception;
	public void Vali_Lbl_MovementsToClarify() throws Exception;
	public void Vali_Lbl_MXN() throws Exception;
	public void Vali_Lbl_Movement_Name() throws Exception;
	public void Vali_Lbl_Date_Movement() throws Exception;
	public void Vali_Btn_One_Movement() throws Exception;
	public void Vali_Btn_Two_Movement() throws Exception;
	
	
	//comprobante validaciones
	
	public void Vali_Lbl_Receipt() throws Exception;
	public void Vali_Lbl_Clarification_Sent() throws Exception;
	public void Vali_Lbl_Fast_Answer() throws Exception;
	public void Vali_Lbl_Folio() throws Exception;
	public void Vali_Lbl_Show_Info_bancaria() throws Exception;
	public void Vali_Btn_Share_Receipt() throws Exception;
	public void Vali_Btn_Save_image() throws Exception;
	
	public void Vali_Lbl_DateAndTime() throws Exception;
	public void Vali_Lbl_DateAndTime_Value() throws Exception;
	public void Vali_Lbl_Commitment_Date() throws Exception;
	public void Vali_Lbl_Commitment_Date_Value() throws Exception;
	public void Vali_Lbl_Clarification_Amount() throws Exception;
	public void Vali_Lbl_Clarification_Amount_Value() throws Exception;
	public void Vali_Lbl_Hide_Info_bancaria() throws Exception;
	
	
	
	
	
	
	
	
	
	
	

	

}