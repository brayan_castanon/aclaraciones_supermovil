package mx.com.santander.suite.test.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.xframium.device.factory.DeviceWebDriver;
import org.xframium.page.Page;
import org.xframium.page.Page.ElementDefinition;

public interface objectRepositoryCerrarSesionAndroid extends Page



{	
	@ElementDefinition
	public static final String CLICKMAS = "clickMas";
	
	public void clickMasCS() throws Exception; 
	public void clickCerrar() throws Exception; 
	public void cerrar() throws Exception;
	public void clickAceptarCerrar() throws Exception;

}



	
	
	
