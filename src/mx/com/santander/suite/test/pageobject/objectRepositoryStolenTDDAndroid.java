package mx.com.santander.suite.test.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import io.appium.java_client.android.AndroidDriver;

import org.xframium.Initializable;
import org.xframium.device.factory.DeviceWebDriver;
import org.xframium.page.Page;
import org.xframium.page.Page.ElementDefinition;

public interface objectRepositoryStolenTDDAndroid extends Page

{	
	@ElementDefinition
	public static final String CLICKTARJETATDD = "clickTarjetaTDD";
	@ElementDefinition
	public static final String CLICKYESLOCK = "clickYESCardLock";
	@ElementDefinition
	public static final String CLICKSTOLENDATE = "clickStolenDate";
	@ElementDefinition
	public static final String CLICKSTOLENDATEINPUT = "clickStolenDateInput";
	@ElementDefinition
	public static final String BTNENTITY = "btnEntity";
	@ElementDefinition
	public static final String BTNCDMX = "btnCDMX";
	@ElementDefinition
	public static final String CLICKNOLOCK = "clickNOCardLock";
	@ElementDefinition
	public static final String CIERRAMODAL = "CerrarModal";
	@ElementDefinition
	public static final String BTNATRAS = "btnAtras";
	
//	Aclaraciones project - Clase para los flujos de Robo
	

	public void clickBtnClarifiMovement() throws Exception;
	
	public void clickBtnRobo() throws Exception;
	
	public void National() throws Exception;
	public void State() throws Exception;
	public void BtnContinuar() throws Exception;
	public void Movement1() throws Exception;
	public void Movement2() throws Exception;
	public void BtnContinuar2() throws Exception;
	public void LblInfoBancaria() throws Exception;
	public void CloseReceipt() throws Exception;
	public void foreign() throws Exception;
}