package mx.com.santander.suite.test.pageobject.spi;

import org.openqa.selenium.By.*;
import org.openqa.selenium.support.Color;
//import java.awt.Color;
import org.openqa.selenium.*;
import org.xframium.Initializable;
import org.xframium.device.factory.DeviceWebDriver;
import org.xframium.page.AbstractPage;
import org.xframium.page.ElementDescriptor;
import org.xframium.page.element.Element;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;

import cucumber.api.java.en.When;
import mx.com.santander.ElementosDOM;
import mx.com.santander.suite.test.pageobject.objectRepositoryInicioSesionAndroid;
import mx.com.santander.suite.test.pageobject.objectRepositorySevenReasonsTDDAndroid;
import mx.com.santander.suite.test.pageobject.objectRepositoryFraudTDDAndroid;

public class objectRepositorySevenReasonsTDDAndroidImpl extends AbstractPage implements objectRepositorySevenReasonsTDDAndroid
{	
	
	private WebDriver driver;
	
	ElementosDOM ele = new ElementosDOM();
	GetAttributes getAtt = new GetAttributes(driver);
	ElementosDOM elements=new ElementosDOM();
	
	

	
	
	public void initializePage() {

	}


	
//	ACLARACIONES FLUJO FRAUDE
	
	public void clickTarjetaTDD() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.RESUMEN_TDD)).click();
			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void clickBtnClarifiMovement() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			System.out.println("Tiempo antes de de dar click en el boton");
			Thread.sleep(20000);
			deviceWebDriver.findElement(By.xpath(ele.BTC_CLARIFY_MOVEMENT)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void LabelDoubleCharge() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(10000);
			deviceWebDriver.findElement(By.xpath(ele.LBL_DOUBLE_CHARGE)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void LabelIncorrectAmount() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(10000);
			deviceWebDriver.findElement(By.xpath(ele.LBL_INCORRECT_AMOUNT)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void LabelUnauthorizedPayment() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(10000);
			deviceWebDriver.findElement(By.xpath(ele.LBL_UNAUTHORIZED_PAYMENT)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	public void LabelCashPayment() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(10000);
			deviceWebDriver.findElement(By.xpath(ele.LBL_CASH_PAYMENT)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void LabelPurchaseOrService() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(10000);
			deviceWebDriver.findElement(By.xpath(ele.LBL_PURCHASE_OR_SERVICE)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void LabelServiceCanceled() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(10000);
			deviceWebDriver.findElement(By.xpath(ele.LBL_SERVICE_CANCELED)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void LabelNoRefund() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(10000);
			deviceWebDriver.findElement(By.xpath(ele.LBL_NO_REFUND)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	
	

	
	
	
	
	
	public void BoxWhatHappened() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(10000);
			deviceWebDriver.findElement(By.xpath(ele.BOX_WHAT_HAPPENED)).sendKeys("prueba automatizada");;
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	
	
	public void National() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.NATIONAL)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void Foreign() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.FOREIGN)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	public void State() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.STATE)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	public void BtnContinuar() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.BTNCONTINUAR)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	public void Movement1() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.MOVEMENT1)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void Movement2() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.MOVEMENT2)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void Movement1TDC() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.MOVEMENT1TDC)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void Movement2TDC() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.MOVEMENT2TDC)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	
	
	
	
	public void BtnContinuar2() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.BTNCONTINUAR2)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	
	
	public void LblInfoBancaria() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(10000);
			deviceWebDriver.findElement(By.xpath(ele.INFOBANCARIA)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void CloseReceipt() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.CLOSERECEIPT)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void ScreenWhatIsTheSituation() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(30000);
			String ele1;
//			�Cu�l es tu situaci�n?
			
			ele1 = getAtt.getTextByXpath(getAtt, elements.LBL_WHAT_IS_THE_SITUATION, driver);
			compara(ele1, elements.LBL_WHAT_IS_THE_SITUATION_TEXT);
			ele1 = getAtt.getColorsByXpath(getAtt, elements.LBL_WHAT_IS_THE_SITUATION, driver);
			compara(ele1, elements.COLOR_f2f2f2);
			ele1 = getAtt.getFontTypeByXpath(getAtt, elements.LBL_WHAT_IS_THE_SITUATION, driver);
			compara(ele1, elements.SANTANDER_HEAD_LINE);
			
//			Aclaraciones por compras en comercios
			
			ele1 = getAtt.getTextByXpath(getAtt, elements.LBL_CLARI_PURCHASE_OR_SERVICE, driver);
			compara(ele1, elements.LBL_CLARI_PURCHASE_OR_SERVICE_TEXT);
			ele1 = getAtt.getColorsByXpath(getAtt, elements.LBL_CLARI_PURCHASE_OR_SERVICE, driver);
			compara(ele1, elements.COLOR_000000);
			ele1 = getAtt.getFontTypeByXpath(getAtt, elements.LBL_CLARI_PURCHASE_OR_SERVICE, driver);
			compara(ele1, elements.SANTANDER_TEXT);
			
//			Los movimientos que por ahora se pueden aclarar 
//			solo son consumos en comercios (compras).
			
			ele1 = getAtt.getTextByXpath(getAtt, elements.LBL_MOVEMENTS_IN_SHOPS, driver);
			compara(ele1, elements.LBL_MOVEMENTS_IN_SHOPS_TEXT);
			ele1 = getAtt.getColorsByXpath(getAtt, elements.LBL_MOVEMENTS_IN_SHOPS, driver);
			compara(ele1, elements.COLOR_000000);
			ele1 = getAtt.getFontTypeByXpath(getAtt, elements.LBL_MOVEMENTS_IN_SHOPS, driver);
			compara(ele1, elements.SANTANDER_TEXT);
			
//			Tengo un cobro que yo no hice
			
			ele1 = getAtt.getTextByXpath(getAtt, elements.LBL_FRAUD, driver);
			compara(ele1, elements.LBL_FRAUD_TEXT);
			ele1 = getAtt.getColorsByXpath(getAtt, elements.LBL_FRAUD, driver);
			compara(ele1, elements.COLOR_000000);
			ele1 = getAtt.getFontTypeByXpath(getAtt, elements.LBL_FRAUD, driver);
			compara(ele1, elements.SANTANDER_HEAD_LINE);
			

//			Me robaron o extravi� mi tarjeta
			
			ele1 = getAtt.getTextByXpath(getAtt, elements.LBL_STOLE, driver);
			compara(ele1, elements.LBL_STOLE_TEXT);
			ele1 = getAtt.getColorsByXpath(getAtt, elements.LBL_STOLE, driver);
			compara(ele1, elements.COLOR_000000);
			ele1 = getAtt.getFontTypeByXpath(getAtt, elements.LBL_STOLE, driver);
			compara(ele1, elements.SANTANDER_HEAD_LINE);
			
			
			
			
			
//			Me hicieron un doble cobro

			
			ele1 = getAtt.getTextByXpath(getAtt, elements.LBL_DOUBLE_CHARGE, driver);
			compara(ele1, elements.LBL_DOUBLE_CHARGE_TEXT);
			ele1 = getAtt.getColorsByXpath(getAtt, elements.LBL_DOUBLE_CHARGE, driver);
			compara(ele1, elements.COLOR_000000);
			ele1 = getAtt.getFontTypeByXpath(getAtt, elements.LBL_DOUBLE_CHARGE, driver);
			compara(ele1, elements.SANTANDER_HEAD_LINE);
			
//			La cantidad que me cobraron es incorrecta
			
			ele1 = getAtt.getTextByXpath(getAtt, elements.LBL_INCORRECT_AMOUNT, driver);
			compara(ele1, elements.LBL_INCORRECT_AMOUNT_TEXT);
			ele1 = getAtt.getColorsByXpath(getAtt, elements.LBL_INCORRECT_AMOUNT, driver);
			compara(ele1, elements.COLOR_000000);
			ele1 = getAtt.getFontTypeByXpath(getAtt, elements.LBL_INCORRECT_AMOUNT, driver);
			compara(ele1, elements.SANTANDER_HEAD_LINE);
			
//			Me cobraron cosas que no autoric�
			
			ele1 = getAtt.getTextByXpath(getAtt, elements.LBL_UNAUTHORIZED_PAYMENT, driver);
			compara(ele1, elements.LBL_UNAUTHORIZED_PAYMENT_TEXT);
			ele1 = getAtt.getColorsByXpath(getAtt, elements.LBL_UNAUTHORIZED_PAYMENT, driver);
			compara(ele1, elements.COLOR_000000);
			ele1 = getAtt.getFontTypeByXpath(getAtt, elements.LBL_UNAUTHORIZED_PAYMENT, driver);
			compara(ele1, elements.SANTANDER_HEAD_LINE);
			
//			Pagu� en efectivo u otra tarjeta
			
			ele1 = getAtt.getTextByXpath(getAtt, elements.LBL_CASH_PAYMENT, driver);
			compara(ele1, elements.LBL_CASH_PAYMENT_TEXT);
			ele1 = getAtt.getColorsByXpath(getAtt, elements.LBL_CASH_PAYMENT, driver);
			compara(ele1, elements.COLOR_000000);
			ele1 = getAtt.getFontTypeByXpath(getAtt, elements.LBL_CASH_PAYMENT, driver);
			compara(ele1, elements.SANTANDER_HEAD_LINE);
			
//			No recib� la compra o servicio
			
			ele1 = getAtt.getTextByXpath(getAtt, elements.LBL_PURCHASE_OR_SERVICE, driver);
			compara(ele1, elements.LBL_PURCHASE_OR_SERVICE_TEXT);
			ele1 = getAtt.getColorsByXpath(getAtt, elements.LBL_PURCHASE_OR_SERVICE, driver);
			compara(ele1, elements.COLOR_000000);
			ele1 = getAtt.getFontTypeByXpath(getAtt, elements.LBL_PURCHASE_OR_SERVICE, driver);
			compara(ele1, elements.SANTANDER_HEAD_LINE);
			
//			Me cobraron un servicio que cancel�
			
			ele1 = getAtt.getTextByXpath(getAtt, elements.LBL_SERVICE_CANCELED, driver);
			compara(ele1, elements.LBL_SERVICE_CANCELED_TEXT);
			ele1 = getAtt.getColorsByXpath(getAtt, elements.LBL_SERVICE_CANCELED, driver);
			compara(ele1, elements.COLOR_000000);
			ele1 = getAtt.getFontTypeByXpath(getAtt, elements.LBL_SERVICE_CANCELED, driver);
			compara(ele1, elements.SANTANDER_HEAD_LINE);
			
//			Regres� lo que compr� y no recib� la devoluci�n
			
			ele1 = getAtt.getTextByXpath(getAtt, elements.LBL_NO_REFUND, driver);
			compara(ele1, elements.LBL_NO_REFUND_TEXT);
			ele1 = getAtt.getColorsByXpath(getAtt, elements.LBL_NO_REFUND, driver);
			compara(ele1, elements.COLOR_000000);
			ele1 = getAtt.getFontTypeByXpath(getAtt, elements.LBL_NO_REFUND, driver);
			compara(ele1, elements.SANTANDER_HEAD_LINE);
			
//			deviceWebDriver.findElement(By.xpath(ele.CLOSERECEIPT)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void LblWhatIsTheSituation() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_WHAT_IS_THE_SITUATION)).getText();
			compara(ele1, elements.LBL_WHAT_IS_THE_SITUATION_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_WHAT_IS_THE_SITUATION)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_WHAT_IS_THE_SITUATION)).getCssValue("color");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			
			Thread.sleep(20000);
			
//			System.out.println("--------- EL TEXTO ES  ------------"+deviceWebDriver.findElement(By.xpath(ele.LBL_WHAT_IS_THE_SITUATION)).getText()+"------");
//			System.out.println("--------- EL TIPO DE LETRA ES  ------------"+deviceWebDriver.findElement(By.xpath(ele.btnMexico)).getCssValue()+"----");
//			System.out.println("--------- EL COLOR ES  ------------"+deviceWebDriver.findElement(By.xpath(ele.btnMexico)).getCssValue("color")+"-------");
			

			
//			�Cu�l es tu situaci�n?
//			Thread.sleep(20000);
//			ele1 = getAtt.getTextByXpath(getAtt, elements.LBL_WHAT_IS_THE_SITUATION, driver);
//			compara(ele1, elements.LBL_WHAT_IS_THE_SITUATION_TEXT);
//			ele1 = getAtt.getColorsByXpath(getAtt, elements.LBL_WHAT_IS_THE_SITUATION, driver);
//			compara(ele1, elements.COLOR_f2f2f2);
//			ele1 = getAtt.getFontTypeByXpath(getAtt, elements.LBL_WHAT_IS_THE_SITUATION, driver);
//			compara(ele1, elements.SANTANDER_HEAD_LINE);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void MeHicieronUnCargoDoble() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(20000);
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_DOUBLE_CHARGE)).getText();
			compara(ele1, elements.LBL_DOUBLE_CHARGE_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_DOUBLE_CHARGE)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			Color color = Color.fromString(driver.findElement(By.xpath(ele.LBL_DOUBLE_CHARGE)).getCssValue("background-color"));
			ele1=color.asHex();
//			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_DOUBLE_CHARGE)).getCssValue("color");
			compara(ele1,  elements.COLOR_000000);
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	//Metodos para la validacion individual de la primera pantalla 
	
	
	public void Vali_lbl_WhatIsTheSituation() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(20000);
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_WHAT_IS_THE_SITUATION)).getText();
			compara(ele1, elements.LBL_WHAT_IS_THE_SITUATION_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_WHAT_IS_THE_SITUATION)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_WHAT_IS_THE_SITUATION)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	
	
	public void Vali_lbl_Clari_PurchaseOrService() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CLARI_PURCHASE_OR_SERVICE)).getText();
			compara(ele1, elements.LBL_CLARI_PURCHASE_OR_SERVICE_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CLARI_PURCHASE_OR_SERVICE)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CLARI_PURCHASE_OR_SERVICE)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_lbl_MovementsInShops() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_MOVEMENTS_IN_SHOPS)).getText();
			compara(ele1, elements.LBL_MOVEMENTS_IN_SHOPS_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_MOVEMENTS_IN_SHOPS)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_MOVEMENTS_IN_SHOPS)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_lbl_Fraud() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_FRAUD)).getText();
			compara(ele1, elements.LBL_FRAUD_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_FRAUD)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_FRAUD)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_lbl_Stole() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_STOLE)).getText();
			compara(ele1, elements.LBL_STOLE_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_STOLE)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_STOLE)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_lbl_DoubleCharge() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_DOUBLE_CHARGE)).getText();
			compara(ele1, elements.LBL_DOUBLE_CHARGE_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_DOUBLE_CHARGE)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_DOUBLE_CHARGE)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_lbl_IncorrectAmount() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_INCORRECT_AMOUNT)).getText();
			compara(ele1, elements.LBL_INCORRECT_AMOUNT_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_INCORRECT_AMOUNT)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_INCORRECT_AMOUNT)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_lbl_UnauthorizedPayment() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_UNAUTHORIZED_PAYMENT)).getText();
			compara(ele1, elements.LBL_UNAUTHORIZED_PAYMENT_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_UNAUTHORIZED_PAYMENT)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_UNAUTHORIZED_PAYMENT)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_lbl_CashPayment() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CASH_PAYMENT)).getText();
			compara(ele1, elements.LBL_CASH_PAYMENT_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CASH_PAYMENT)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CASH_PAYMENT)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_lbl_PurchaseOrService() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_PURCHASE_OR_SERVICE)).getText();
			compara(ele1, elements.LBL_PURCHASE_OR_SERVICE_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_PURCHASE_OR_SERVICE)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_PURCHASE_OR_SERVICE)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_lbl_ServiceCanceled() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SERVICE_CANCELED)).getText();
			compara(ele1, elements.LBL_SERVICE_CANCELED_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SERVICE_CANCELED)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SERVICE_CANCELED)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_lbl_NoRefund() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_NO_REFUND)).getText();
			compara(ele1, elements.LBL_NO_REFUND_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_NO_REFUND)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_NO_REFUND)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	//VALIDACIONES SEGUNDA PANTALLA
	
	public void Vali_lbl_Questionnaire() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_QUESTIONNAIRE)).getText();
			compara(ele1, elements.LBL_QUESTIONNAIRE_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_QUESTIONNAIRE)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_QUESTIONNAIRE)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_lbl_Double_Charge() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getText();
			compara(ele1, elements.LBL_SCREEN2_DOUBLE_CHARGE_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_lbl_Incorrect_Amount() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getText();
			compara(ele1, elements.LBL_SCREEN2_INCORRECT_AMOUNT_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_lbl_Unauthorized_Payment() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getText();
			compara(ele1, elements.LBL_SCREEN2_UNAUTHORIZED_PAYMENT_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_lbl_Cash_Payment() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getText();
			compara(ele1, elements.LBL_SCREEN2_CASH_PAYMENT_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_lbl_Purchase_Or_Service() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getText();
			compara(ele1, elements.LBL_SCREEN2_PURCHASE_OR_SERVICE_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_lbl_Service_Canceled() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getText();
			compara(ele1, elements.LBL_SCREEN2_SERVICE_CANCELED_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_lbl_Label_NoRefund() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getText();
			compara(ele1, elements.LBL_SCREEN2_NO_REFUND_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SCREEN2_SEVEN_REASONS)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	
	

	
	
	
	public void Vali_lbl_What_Happened() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_WHAT_HAPPENED)).getText();
			compara(ele1, elements.LBL_WHAT_HAPPENED_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_WHAT_HAPPENED)).getCssValue("font-family");
			compara(ele1,  elements.OPENSANS);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_WHAT_HAPPENED)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	 
	
	 
	
	
	public void Vali_lbl_Current_Place() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CURRENT_PLACE)).getText();
			compara(ele1, elements.LBL_CURRENT_PLACE_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CURRENT_PLACE)).getCssValue("font-family");
			compara(ele1,  elements.OPENSANS);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CURRENT_PLACE)).getCssValue("color");
			ele1=colorRgbaToHexa(ele1);
			compara(ele1,elements.COLOR_7676767);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_lbl_Where_Are_You() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_WHERE_ARE_YOU)).getText();
			compara(ele1, elements.LBL_WHERE_ARE_YOU_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_WHERE_ARE_YOU)).getCssValue("font-family");
			compara(ele1,  elements.OPENSANS);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_WHERE_ARE_YOU)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Btn_National() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTN_SCREEN2_NATIONAL)).getText();
			compara(ele1, elements.BTN_SCREEN2_NATIONAL_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTN_SCREEN2_NATIONAL)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTN_SCREEN2_NATIONAL)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Btn_Foreign() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTN_SCREEN2_FOREIGN)).getText();
			compara(ele1, elements.BTN_SCREEN2_FOREIGN_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTN_SCREEN2_FOREIGN)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTN_SCREEN2_FOREIGN)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Lbl_Federal_Entity() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_FEDERAL_ENTITY)).getText();
			compara(ele1, elements.LBL_FEDERAL_ENTITY_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_FEDERAL_ENTITY)).getCssValue("font-family");
			compara(ele1,  elements.OPENSANS);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_FEDERAL_ENTITY)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Btn_Continuar() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTNCONTINUAR)).getText();
			compara(ele1, elements.LBL_CONTINUAR_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTNCONTINUAR)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTNCONTINUAR)).getCssValue("color");
			ele1=colorRgbaToHexa(ele1);
			compara(ele1,elements.COLOR_ffffff);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	//VALIDACIONES TERCERA PANTALLA
	
 
	
	public void Vali_Lbl_Selection_Movements() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SELECTION_MOVEMENTS)).getText();
			compara(ele1, elements.LBL_SELECTION_MOVEMENTS_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SELECTION_MOVEMENTS)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SELECTION_MOVEMENTS)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_Lbl_CardName() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CARD_NAME)).getText();
			compara(ele1, elements.LBL_CARD_NAME_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CARD_NAME)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CARD_NAME)).getCssValue("color");
			ele1=colorRgbaToHexa(ele1);
			compara(ele1,elements.COLOR_7676767);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_Btn_Filtrar() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTN_FILTRAR)).getText();
			compara(ele1, elements.BTN_FILTRAR_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTN_FILTRAR)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTN_FILTRAR)).getCssValue("color");
			ele1=colorRgbaToHexa(ele1);
			compara(ele1,elements.COLOR_ffffff);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_Lbl_Select_Movements() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SELECT_MOVEMENTS)).getText();
			compara(ele1, elements.LBL_SELECT_MOVEMENTS_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SELECT_MOVEMENTS)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SELECT_MOVEMENTS)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_Lbl_MovementsToClarify() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_MOVEMENTS_TO_CLARIFY)).getText();
			compara(ele1, elements.LBL_MOVEMENTS_TO_CLARIFY_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_MOVEMENTS_TO_CLARIFY)).getCssValue("font-family");
			compara(ele1,  elements.OPENSANS);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_MOVEMENTS_TO_CLARIFY)).getCssValue("color");
			ele1=colorRgbaToHexa(ele1);
			compara(ele1,elements.COLOR_7676767);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_Lbl_MXN() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_MXN)).getText();
			compara(ele1, elements.LBL_MXN_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_MXN)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_MXN)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_Lbl_Movement_Name() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_MOVEMENT_NAME)).getText();
			compara(ele1, elements.LBL_MOVEMENT_NAME_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_MOVEMENT_NAME)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_MOVEMENT_NAME)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_Lbl_Date_Movement() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_DATE_MOVEMENT)).getText();
			compara(ele1, elements.LBL_DATE_MOVEMENT_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_DATE_MOVEMENT)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_DATE_MOVEMENT)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	public void Vali_Btn_One_Movement() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTNCONTINUAR2)).getText();
			compara(ele1, elements.BTN_ONE_MOVEMENT_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTNCONTINUAR2)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTNCONTINUAR2)).getCssValue("color");
			ele1=colorRgbaToHexa(ele1);
			compara(ele1,elements.COLOR_ffffff);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Btn_Two_Movement() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTNCONTINUAR2)).getText();
			compara(ele1, elements.BTN_TWO_MOVEMENT_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTNCONTINUAR2)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTNCONTINUAR2)).getCssValue("color");
			ele1=colorRgbaToHexa(ele1);
			compara(ele1,elements.COLOR_ffffff);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	//validaciones del comprobante
	
	public void Vali_Lbl_Receipt() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_RECEIPT)).getText();
			compara(ele1, elements.LBL_RECEIPT_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_RECEIPT)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_RECEIPT)).getCssValue("color");
//			ele1=colorRgbaToHexa(ele1);
			compara(ele1,elements.RGBA_0_0_0_087);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Lbl_Clarification_Sent() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CLARIFICATIONS_SENT)).getText();
			compara(ele1, elements.LBL_CLARIFICATIONS_SENT_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CLARIFICATIONS_SENT)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CLARIFICATIONS_SENT)).getCssValue("color");
//			ele1=colorRgbaToHexa(ele1);
			compara(ele1,elements.RGBA_0_0_0_087);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Lbl_Fast_Answer() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_FAST_ANSWER)).getText();
			compara(ele1, elements.LBL_FAST_ANSWER_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_FAST_ANSWER)).getCssValue("font-family");
			compara(ele1,  elements.OPENSANS);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_FAST_ANSWER)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Lbl_Folio() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_FOLIO)).getText();
			compara(ele1, elements.LBL_FOLIO_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_FOLIO)).getCssValue("font-family");
			compara(ele1,  elements.OPENSANS);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_FOLIO)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Lbl_Show_Info_bancaria() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SHOW_INFO_BANCARIA)).getText();
			compara(ele1, elements.LBL_SHOW_INFO_BANCARIA_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SHOW_INFO_BANCARIA)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_SHOW_INFO_BANCARIA)).getCssValue("color");
			ele1=colorRgbaToHexa(ele1);
			compara(ele1,elements.COLOR_ec00);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Btn_Share_Receipt() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTN_SHARE_RECEIPT)).getText();
			compara(ele1, elements.BTN_SHARE_RECEIPT_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTN_SHARE_RECEIPT)).getCssValue("font-family");
			compara(ele1,  elements.OPENSANS);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTN_SHARE_RECEIPT)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Btn_Save_image() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTN_SAVE_IMAGE)).getText();
			compara(ele1, elements.BTN_SAVE_IMAGE_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTN_SAVE_IMAGE)).getCssValue("font-family");
			compara(ele1,  elements.OPENSANS);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.BTN_SAVE_IMAGE)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Lbl_DateAndTime() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_DATE_AND_TIME)).getText();
			compara(ele1, elements.LBL_DATE_AND_TIME_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_DATE_AND_TIME)).getCssValue("font-family");
			compara(ele1,  elements.OPENSANS);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_DATE_AND_TIME)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Lbl_DateAndTime_Value() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_DATE_AND_TIME_VALUE)).getText();
			compara(ele1, elements.LBL_DATE_AND_TIME_VALUE_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_DATE_AND_TIME_VALUE)).getCssValue("font-family");
			compara(ele1,  elements.OPENSANS);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_DATE_AND_TIME_VALUE)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Lbl_Commitment_Date() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_COMMITMENT_DATE)).getText();
			compara(ele1, elements.LBL_COMMITMENT_DATE_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_COMMITMENT_DATE)).getCssValue("font-family");
			compara(ele1,  elements.OPENSANS);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_COMMITMENT_DATE)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Lbl_Commitment_Date_Value() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_COMMITMENT_DATE_VALUE)).getText();
			compara(ele1, elements.LBL_COMMITMENT_DATE_VALUE_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_COMMITMENT_DATE_VALUE)).getCssValue("font-family");
			compara(ele1,  elements.OPENSANS);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_COMMITMENT_DATE_VALUE)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Lbl_Clarification_Amount() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CLARIFICATIONS_AMOUNT)).getText();
			compara(ele1, elements.LBL_CLARIFICATIONS_AMOUNT_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CLARIFICATIONS_AMOUNT)).getCssValue("font-family");
			compara(ele1,  elements.OPENSANS);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CLARIFICATIONS_AMOUNT)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Lbl_Clarification_Amount_Value() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CLARIFICATIONS_AMOUNT_VALUE)).getText();
			compara(ele1, elements.LBL_CLARIFICATIONS_AMOUNT_VALUE_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CLARIFICATIONS_AMOUNT_VALUE)).getCssValue("font-family");
			compara(ele1,  elements.OPENSANS);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_CLARIFICATIONS_AMOUNT_VALUE)).getCssValue("color");
			ele1=colorRgbaToHexa000000(ele1);
			compara(ele1,elements.COLOR_000000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public void Vali_Lbl_Hide_Info_bancaria() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_HIDE_INFO_BANCARIA)).getText();
			compara(ele1, elements.LBL_HIDE_INFO_BANCARIA_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_HIDE_INFO_BANCARIA)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_HIDE_INFO_BANCARIA)).getCssValue("color");
			ele1=colorRgbaToHexa(ele1);
			compara(ele1,elements.COLOR_ec0000);
			
			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	
	
	public String colorRgbaToHexa000000(String colorRgba) {
		String[] numbers = colorRgba.replace("rgba(", "").replace(")", "").split(",");
		int r = Integer.parseInt(numbers[0].trim());
		int g = Integer.parseInt(numbers[1].trim());
		int b = Integer.parseInt(numbers[2].trim());
		int a = Integer.parseInt(numbers[3].trim());
//		System.out.println("r: " + r + "g: " + g + "b: " + b+ "a: " + a);
		String hex = "#" + Integer.toHexString(r) + Integer.toHexString(g) + Integer.toHexString(b)+0+0+0;
//		System.out.println(hex);
		return hex;
		
		
	}
	
	public String colorRgbaToHexa(String colorRgba) {
		String[] numbers = colorRgba.replace("rgba(", "").replace(")", "").split(",");
		int r = Integer.parseInt(numbers[0].trim());
		int g = Integer.parseInt(numbers[1].trim());
		int b = Integer.parseInt(numbers[2].trim());
		int a = Integer.parseInt(numbers[3].trim());
//		System.out.println("r: " + r + "g: " + g + "b: " + b+ "a: " + a);
		String hex = "#" + Integer.toHexString(r) + Integer.toHexString(g) + Integer.toHexString(b);
//		System.out.println(hex);
		return hex;
	
	
	}
	

	
	
	
	
	public void compara(String attrib1, String attrib2) throws Exception 
	{
		
		int res = attrib1.compareTo(attrib2);
//		String resultado = null;
		
		if(res == 0) 
		{
			System.out.println("El valor obtenido es: " + attrib1 + " El valor esperado es: " + attrib2);
			System.out.println("El resultado de la comparacion es EXITOSA");
//			resultado = "El resultado de la comparacion es EXITOSA: " + "Se obtuvo: " + attrib1 + " Se esperaba: " + attrib2;
		}else 
		{
			System.out.println("El valor obtenido es: " + attrib1 + " El valor esperado es: " + attrib2);
			System.out.println("El resultado de la comparacion es FALLIDA");
//			resultado = "El resultado de la comparacion es FALLIDA: " + "Se obtuvo: " + attrib1 + " Se esperaba: " + attrib2;
		}
//		return resultado;
	}
	
	
	
		
}
