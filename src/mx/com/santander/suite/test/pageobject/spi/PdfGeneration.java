package mx.com.santander.suite.test.pageobject.spi;

import java.io.FileOutputStream;
import java.sql.Date;

import org.openqa.selenium.WebDriver;

import com.itextpdf.text.Document;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Element;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import mx.com.santander.Fecha;
import mx.com.santander.Fuentes;
import mx.com.santander.Rutas;
import mx.com.santander.ScreenShots;

public class PdfGeneration {
		
		ScreenShots captura = new ScreenShots();
		Document document = new Document();
	//	java.util.Date fecha = new Date();
		Rutas rutas = new Rutas();
		Fecha nameFecha = new Fecha();
		Fuentes estilos = new Fuentes();
//		DriverTest testDriver = new DriverTest();
		//Eventos eventos = new Eventos();
		 // Este codigo genera una tabla de 4 columnas
	    PdfPTable table = new PdfPTable(4); 
	    PdfPTable screen2 = new PdfPTable(4); 
	    PdfPTable screen3 = new PdfPTable(4); 
	    PdfPTable screen4 = new PdfPTable(4);
	    PdfPTable screen5 = new PdfPTable(4);
	    PdfPTable screen6 = new PdfPTable(4);
	    PdfPTable screen7 = new PdfPTable(4);
	    PdfPTable screen8 = new PdfPTable(4);
	    PdfPTable screen9 = new PdfPTable(4);
	    PdfPTable screen10 = new PdfPTable(4);
	    PdfPTable screen11 = new PdfPTable(4);
	    PdfPTable screen12 = new PdfPTable(4);
		int cont = 0;
		
		Font  fuente = estilos.styles();
		String File = rutas.EVIDENCIAS2 + nameFecha.name() + ".pdf";
		

		public void instancia () throws Exception{
			PdfWriter.getInstance(document, new FileOutputStream(File));
		}
		
		public void open () throws Exception{

			document.open();
			System.out.println("Abriendo documento");
			
		}
		
		public void status (){
			Boolean abierto = document.isOpen();
			System.out.println(abierto);
		}
			
		public  Image encabezado () throws Exception {
			Image cabecera = Image.getInstance("encabezado\\cabecera.png");
			cabecera.scaleAbsoluteWidth(510f);
			cabecera.scaleAbsoluteHeight(130f);
			cabecera.setAlignment(Element.ALIGN_CENTER);
			return cabecera;	
		}

		public void parrafo (String texto)throws Exception {
			document.add(new Paragraph(texto, fuente ));	
		}
		
		public void agregaImg (Image imagen) throws Exception {
//			System.out.println("aqui esta la imagen: "+ imagen);
			document.add(imagen);
//			System.out.println("aqui esta la imagen 2: "+ imagen);
		}
		
		public Image imageEvidencia (String name) throws Exception{
			Image captura = Image.getInstance("Report\\Screen\\"+ name + ".png");
//			System.out.println("aqui esta la imagen 2: "+ captura);
			captura.scaleAbsoluteWidth(560f);
			captura.scaleAbsoluteHeight(340f);
			captura.setAlignment(Element.ALIGN_CENTER);
			
			return captura;
		}
		
		public void nuevaPagina (){
			Boolean abierto = document.isOpen();
			System.out.println("nueva: " + abierto);
			document.newPage();
		}
		
		public void add (WebDriver driver) throws Exception {
			captura.takeScreenShotTest(driver, "IMAGEN00" );
			agregaImg(imageEvidencia("IMAGEN00"));
		}
		
		
		public void addCeldaCabecera (WebDriver driver) throws Exception {
			table.addCell("TEXTO OBTENIDO");
			table.addCell("COLOR  LETRA");
	        table.addCell("TIPO DE LETRA ");
	        table.addCell("FONDO ELEMENTO");
		}
		
		public void addCeldaCabeceraScreen2 (WebDriver driver) throws Exception {
			screen2.addCell("TEXTO OBTENIDO");
			screen2.addCell("COLOR  LETRA");
			screen2.addCell("TIPO DE LETRA ");
			screen2.addCell("FONDO ELEMENTO");
		}
		
		public void addCeldaCabeceraScreen3 (WebDriver driver) throws Exception {
			screen3.addCell("TEXTO OBTENIDO");
			screen3.addCell("COLOR  LETRA");
			screen3.addCell("TIPO DE LETRA ");
			screen3.addCell("FONDO ELEMENTO");
		}
		
		public void addCeldaCabeceraScreen4 (WebDriver driver) throws Exception {
			screen4.addCell("TEXTO OBTENIDO");
			screen4.addCell("COLOR  LETRA");
			screen4.addCell("TIPO DE LETRA ");
			screen4.addCell("FONDO ELEMENTO");
		}
		
		public void addCeldaCabeceraScreen5 (WebDriver driver) throws Exception {
			screen5.addCell("TEXTO OBTENIDO");
			screen5.addCell("COLOR  LETRA");
			screen5.addCell("TIPO DE LETRA ");
			screen5.addCell("FONDO ELEMENTO");
		}
		
		public void addCeldaCabeceraScreen6 (WebDriver driver) throws Exception {
			screen6.addCell("TEXTO OBTENIDO");
			screen6.addCell("COLOR  LETRA");
			screen6.addCell("TIPO DE LETRA ");
			screen6.addCell("FONDO ELEMENTO");
		}
		
		public void addCeldaCabeceraScreen7 (WebDriver driver) throws Exception {
			screen7.addCell("TEXTO OBTENIDO");
			screen7.addCell("COLOR  LETRA");
			screen7.addCell("TIPO DE LETRA ");
			screen7.addCell("FONDO ELEMENTO");
		}
		
		public void addCeldaCabeceraScreen8 (WebDriver driver) throws Exception {
			screen8.addCell("TEXTO OBTENIDO");
			screen8.addCell("COLOR  LETRA");
			screen8.addCell("TIPO DE LETRA ");
			screen8.addCell("FONDO ELEMENTO");
		}
		
		public void addCeldaCabeceraScreen9 (WebDriver driver) throws Exception {
			screen9.addCell("TEXTO OBTENIDO");
			screen9.addCell("COLOR  LETRA");
			screen9.addCell("TIPO DE LETRA ");
			screen9.addCell("FONDO ELEMENTO");
		}
		
		public void addCeldaCabeceraScreen10 (WebDriver driver) throws Exception {
			screen10.addCell("TEXTO OBTENIDO");
			screen10.addCell("COLOR  LETRA");
			screen10.addCell("TIPO DE LETRA ");
			screen10.addCell("FONDO ELEMENTO");
		}
		public void addCeldaCabeceraScreen11 (WebDriver driver) throws Exception {
			screen10.addCell("TEXTO OBTENIDO");
			screen10.addCell("COLOR  LETRA");
			screen10.addCell("TIPO DE LETRA ");
			screen10.addCell("FONDO ELEMENTO");
		}
		public void addCeldaCabeceraScreen12 (WebDriver driver) throws Exception {
			screen10.addCell("TEXTO OBTENIDO");
			screen10.addCell("COLOR  LETRA");
			screen10.addCell("TIPO DE LETRA ");
			screen10.addCell("FONDO ELEMENTO");
		}
		
		
		
		public void addCelda (String valOne) throws Exception {
//			 table.addCell(valOne);
			PdfPCell cell = new PdfPCell(new Phrase(valOne));
			BaseColor ok = WebColors.getRGBColor("#33CC00");
			BaseColor nok = WebColors.getRGBColor("#FF3333");
			
			if(valOne.contains("FALLIDA")) 
			{
				cell.setBackgroundColor(nok);
				table.addCell(cell);
			}else {
				cell.setBackgroundColor(ok);
				table.addCell(cell);
			}
	 	}
		
	    public void addScreen2 (String valOne) throws Exception {
			
//	        screen2.addCell(valOne);
	    	PdfPCell cell = new PdfPCell(new Phrase(valOne));
			BaseColor ok = WebColors.getRGBColor("#33CC00");
			BaseColor nok = WebColors.getRGBColor("#FF3333");
			
			if(valOne.contains("FALLIDA")) 
			{
				cell.setBackgroundColor(nok);
				screen2.addCell(cell);
			}else {
				cell.setBackgroundColor(ok);
				screen2.addCell(cell);
			}
	 	}
	    
	   public void addScreen3 (String valOne) throws Exception {
			
//	        screen3.addCell(valOne);
		   PdfPCell cell = new PdfPCell(new Phrase(valOne));
			BaseColor ok = WebColors.getRGBColor("#33CC00");
			BaseColor nok = WebColors.getRGBColor("#FF3333");
			
			if(valOne.contains("FALLIDA")) 
			{
				cell.setBackgroundColor(nok);
				screen3.addCell(cell);
			}else {
				cell.setBackgroundColor(ok);
				screen3.addCell(cell);
			}
	 	}
	    
	  public void addScreen4 (String valOne) throws Exception {
			
//	        screen4.addCell(valOne);
		  PdfPCell cell = new PdfPCell(new Phrase(valOne));
			BaseColor ok = WebColors.getRGBColor("#33CC00");
			BaseColor nok = WebColors.getRGBColor("#FF3333");
			
			if(valOne.contains("FALLIDA")) 
			{
				cell.setBackgroundColor(nok);
				screen4.addCell(cell);
			}else {
				cell.setBackgroundColor(ok);
				screen4.addCell(cell);
			}
	 	}
	  
	  public void addScreen5 (String valOne) throws Exception {
			
//	      screen5.addCell(valOne);
		  PdfPCell cell = new PdfPCell(new Phrase(valOne));
			BaseColor ok = WebColors.getRGBColor("#33CC00");
			BaseColor nok = WebColors.getRGBColor("#FF3333");
			
			if(valOne.contains("FALLIDA")) 
			{
				cell.setBackgroundColor(nok);
				screen5.addCell(cell);
			}else {
				cell.setBackgroundColor(ok);
				screen5.addCell(cell);
			}
		}
	  
	  public void addScreen6 (String valOne) throws Exception {
			
//	      screen6.addCell(valOne);
		  PdfPCell cell = new PdfPCell(new Phrase(valOne));
			BaseColor ok = WebColors.getRGBColor("#33CC00");
			BaseColor nok = WebColors.getRGBColor("#FF3333");
			
			if(valOne.contains("FALLIDA")) 
			{
				cell.setBackgroundColor(nok);
				screen6.addCell(cell);
			}else {
				cell.setBackgroundColor(ok);
				screen6.addCell(cell);
			}
		}
	  
	  public void addScreen7 (String valOne) throws Exception {
			
//	      screen7.addCell(valOne);
		  PdfPCell cell = new PdfPCell(new Phrase(valOne));
			BaseColor ok = WebColors.getRGBColor("#33CC00");
			BaseColor nok = WebColors.getRGBColor("#FF3333");
			
			if(valOne.contains("FALLIDA")) 
			{
				cell.setBackgroundColor(nok);
				screen7.addCell(cell);
			}else {
				cell.setBackgroundColor(ok);
				screen7.addCell(cell);
			}
		}
	  
	  public void addScreen8 (String valOne) throws Exception {
			
//	      screen8.addCell(valOne);
		  PdfPCell cell = new PdfPCell(new Phrase(valOne));
			BaseColor ok = WebColors.getRGBColor("#33CC00");
			BaseColor nok = WebColors.getRGBColor("#FF3333");
			
			if(valOne.contains("FALLIDA")) 
			{
				cell.setBackgroundColor(nok);
				screen8.addCell(cell);
			}else {
				cell.setBackgroundColor(ok);
				screen8.addCell(cell);
			}
	  }
	  
	  public void addScreen9 (String valOne) throws Exception {
			
//	      screen9.addCell(valOne);
		  PdfPCell cell = new PdfPCell(new Phrase(valOne));
			BaseColor ok = WebColors.getRGBColor("#33CC00");
			BaseColor nok = WebColors.getRGBColor("#FF3333");
			
			if(valOne.contains("FALLIDA")) 
			{
				cell.setBackgroundColor(nok);
				screen9.addCell(cell);
			}else {
				cell.setBackgroundColor(ok);
				screen9.addCell(cell);
			}
	  }
	  
	  public void addScreen10 (String valOne) throws Exception {
			
//	      screen10.addCell(valOne);
		  PdfPCell cell = new PdfPCell(new Phrase(valOne));
			BaseColor ok = WebColors.getRGBColor("#33CC00");
			BaseColor nok = WebColors.getRGBColor("#FF3333");
			
			if(valOne.contains("FALLIDA")) 
			{
				cell.setBackgroundColor(nok);
				screen10.addCell(cell);
			}else {
				cell.setBackgroundColor(ok);
				screen10.addCell(cell);
			}
	  }
	  public void addScreen11 (String valOne) throws Exception {
			
//	      screen11.addCell(valOne);
		  PdfPCell cell = new PdfPCell(new Phrase(valOne));
			BaseColor ok = WebColors.getRGBColor("#33CC00");
			BaseColor nok = WebColors.getRGBColor("#FF3333");
			
			if(valOne.contains("FALLIDA")) 
			{
				cell.setBackgroundColor(nok);
				screen11.addCell(cell);
			}else {
				cell.setBackgroundColor(ok);
				screen11.addCell(cell);
			}
	  }
	  public void addScreen12 (String valOne) throws Exception {
			
//	      screen12.addCell(valOne);
		  PdfPCell cell = new PdfPCell(new Phrase(valOne));
			BaseColor ok = WebColors.getRGBColor("#33CC00");
			BaseColor nok = WebColors.getRGBColor("#FF3333");
			
			if(valOne.contains("FALLIDA")) 
			{
				cell.setBackgroundColor(nok);
				screen12.addCell(cell);
			}else {
				cell.setBackgroundColor(ok);
				screen12.addCell(cell);
			}
	  }
	    
	    	
		public void addTable (WebDriver driver) throws Exception {
			 document.add(table);
		}
		
		public void addTableScreen2 (WebDriver driver) throws Exception {
			 document.add(screen2);
		}
		
		public void addTableScreen3 (WebDriver driver) throws Exception {
			 document.add(screen3);
		}
		
		public void addTableScreen4 (WebDriver driver) throws Exception {
			 document.add(screen4);
		}
		
		public void addTableScreen5 (WebDriver driver) throws Exception {
			 document.add(screen5);
		}
		
		public void addTableScreen6 (WebDriver driver) throws Exception {
			 document.add(screen6);
		}
		
		public void addTableScreen7 (WebDriver driver) throws Exception {
			 document.add(screen7);
		}
		
		public void addTableScreen8 (WebDriver driver) throws Exception {
			 document.add(screen8);
		}
		
		public void addTableScreen9 (WebDriver driver) throws Exception {
			 document.add(screen9);
		}
		
		public void addTableScreen10 (WebDriver driver) throws Exception {
			 document.add(screen10);
		}
		
		public void addTableScreen11 (WebDriver driver) throws Exception {
			 document.add(screen11);
		}
		
		public void addTableScreen12 (WebDriver driver) throws Exception {
			 document.add(screen12);
		}
		
		
		
		
		
		public void close (){
			System.out.println("cerrando");
			document.close();
		}




	}




