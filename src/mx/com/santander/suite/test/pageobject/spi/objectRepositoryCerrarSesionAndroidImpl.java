package mx.com.santander.suite.test.pageobject.spi;



import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import org.xframium.Initializable;
import org.xframium.device.factory.DeviceWebDriver;

import java.io.*; 
import org.apache.poi.hssf.usermodel.HSSFWorkbook; 
import org.apache.poi.ss.usermodel.Cell; 
import org.apache.poi.ss.usermodel.Row; 
import org.apache.poi.ss.usermodel.Sheet; 
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.By;
import org.xframium.page.AbstractPage;

import mx.com.santander.ElementosDOM;
import mx.com.santander.suite.libraries.ManagementExcel;
import mx.com.santander.suite.test.pageobject.objectRepositoryCerrarSesionAndroid;
import mx.com.santander.suite.test.pageobject.objectRepositoryInicioSesionAndroid;



public class objectRepositoryCerrarSesionAndroidImpl extends AbstractPage implements objectRepositoryCerrarSesionAndroid
{	
	ElementosDOM ele = new ElementosDOM();
	
	
	public void initializePage()
    {
    }
	

	public void clickMasCS() throws Exception {
		
		DeviceWebDriver deviceWebDriver = null;
		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(10000);
			deviceWebDriver.findElement(By.xpath(ele.C_BTN_MAS)).click();
			Thread.sleep(5000);
			
		}catch (Exception e) {
			System.out.println("Existe un error! Al dar click en Mas");
		}
		
	}

	public void cerrar() throws Exception {
		
		DeviceWebDriver deviceWebDriver = null;
		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(10000);
			deviceWebDriver.findElement(By.xpath(ele.C_CLICK_CERRAR_VAYA)).click();
			Thread.sleep(5000);
			
		}catch (Exception e) {
			System.out.println("Existe un error! Al dar click en cerrar");
		}
		
	}
	
	
	public void clickCerrar() throws Exception {
		DeviceWebDriver deviceWebDriver = null;
		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.C_BTN_CERRAR)).click();
			Thread.sleep(5000);
			
		}catch (Exception e) {
			System.out.println("Existe un error al dar click en Cerrar Sesion");
		
		}
	}


	public void clickAceptarCerrar() throws Exception {
		DeviceWebDriver deviceWebDriver = null;
		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.C_BTN_POP_ACEPTO)).click();
			Thread.sleep(5000);
			
		}catch (Exception e) {
			System.out.println("Existe un error al dar click en Aceptar Cerrar Sesion");
		
		
	}
	
	}
	
		

}
