package mx.com.santander.suite.test.pageobject.spi;



import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import org.xframium.Initializable;
import org.xframium.device.factory.DeviceWebDriver;

import java.io.*; 
import org.apache.poi.hssf.usermodel.HSSFWorkbook; 
import org.apache.poi.ss.usermodel.Cell; 
import org.apache.poi.ss.usermodel.Row; 
import org.apache.poi.ss.usermodel.Sheet; 
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.Color;
import org.xframium.page.AbstractPage;

import mx.com.santander.ElementosDOM;
import mx.com.santander.suite.libraries.ManagementExcel;
import mx.com.santander.suite.test.pageobject.objectRepositoryCerrarSesionAndroid;
import mx.com.santander.suite.test.pageobject.objectRepositoryInicioSesionAndroid;
import mx.com.santander.suite.test.pageobject.objectRepositoryResumenTDDAndroid;



public class objectRepositoryResumenTDDAndroidImpl extends AbstractPage implements objectRepositoryResumenTDDAndroid
{	
private WebDriver driver;
	
	ElementosDOM ele = new ElementosDOM();
	GetAttributes getAtt = new GetAttributes(driver);
	ElementosDOM elements=new ElementosDOM();
	
	
	public void initializePage()
    {
    }
	
	
	
	public void LabelUltimosMovimientos() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(20000);
			String ele1=null;
			
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_ULTIMOS_MOVIMIENTOS)).getText();
			compara(ele1, elements.LBL_DOUBLE_CHARGE_TEXT);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_ULTIMOS_MOVIMIENTOS)).getCssValue("font-family");
			compara(ele1,  elements.SANTANDER_HEAD_LINE);
			ele1 = deviceWebDriver.findElement(By.xpath(ele.LBL_ULTIMOS_MOVIMIENTOS)).getCssValue("color");
			compara(ele1,elements.COLOR_000000);

			
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento"+e);
		} 
	}
	
	public String compara(String attrib1, String attrib2) throws Exception 
	{
		
		int res = attrib1.compareTo(attrib2);
		String resultado = null;
		
		if(res == 0) 
		{
			System.out.println("El valor obtenido es: " + attrib1 + " El valor esperado es: " + attrib2);
			resultado = "El resultado de la comparacion es EXITOSA: " + "Se obtuvo: " + attrib1 + " Se esperaba: " + attrib2;
		}else 
		{
			System.out.println("El valor obtenido es: " + attrib1 + " El valor esperado es: " + attrib2);
			resultado = "El resultado de la comparacion es FALLIDA: " + "Se obtuvo: " + attrib1 + " Se esperaba: " + attrib2;
		}
		return resultado;
	}
	
	public String getColorByXpath(String elemento, WebDriver driver) 
	{
		Color color = Color.fromString(driver.findElement(By.xpath(elemento)).getCssValue("color"));
		
		return color.asHex();

	}
	
	

	
	
		

}
