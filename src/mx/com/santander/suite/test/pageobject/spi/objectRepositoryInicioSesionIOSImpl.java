package mx.com.santander.suite.test.pageobject.spi;



import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

import org.openqa.selenium.By;
import org.xframium.Initializable;
import org.xframium.device.factory.DeviceWebDriver;
import org.xframium.page.AbstractPage;

import mx.com.santander.ElementosDOM;
import mx.com.santander.suite.test.pageobject.objectRepositoryInicioSesionAndroid;
import mx.com.santander.suite.test.pageobject.objectRepositoryInicioSesionIOS;





public class objectRepositoryInicioSesionIOSImpl extends AbstractPage implements objectRepositoryInicioSesionIOS
{	
	ElementosDOM ele = new ElementosDOM();
	
	public void initializePage()
    {
    }
	
	public void clickIngresar() throws Exception {
		getElement(objectRepositoryInicioSesionIOS.INGRESAR_1).click();
	}

	public void setIngresarNumCliente(String usuario) throws Exception {
		getElement(objectRepositoryInicioSesionIOS.CODCLIENTE).click();
		DeviceWebDriver deviceWebDriver = null;

		
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
//			deviceWebDriver.findElement(By.xpath(ele.btn_codCliente)).click();
			for(int i = 0; i < 9; i++) 
			{
				deviceWebDriver.findElement(By.xpath(ele.BTN_BORRAR_BUC)).click();
			}
		
		getElement(objectRepositoryInicioSesionIOS.CODCLIENTE).setValue(usuario, Initializable.xFID.get());
	}

	public void clickContinuar() throws Exception {
		getElement(objectRepositoryInicioSesionIOS.BTNCONTINUAR).click();
		
	}

	public void setPassword(String password) throws Exception {
		getElement(objectRepositoryInicioSesionIOS.PASSWORD).setValue(password, Initializable.xFID.get());
//		getElement(objectRepositoryInicioSesionIOS.BTNOK).click();
	}

	public void clickContinuar2() throws Exception {
		getElement(objectRepositoryInicioSesionIOS.BTNCONTINUAR2).click();
		
	}
	
	
	public void clickDone() throws Exception {
		getElement(objectRepositoryInicioSesionIOS.BTNDONE).click();
		
	}
	
	
	public void clickDone1() throws Exception {
		getElement(objectRepositoryInicioSesionIOS.BTNDONE1).click();
		
	}
	
	
	public void clickProductos() throws Exception {
		getElement(objectRepositoryInicioSesionIOS.PRODUCTOS).click();
		
	}
	
		
	public void borrar() throws Exception {
		System.out.println("Soy el metodo de borrar el buc");
		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
//			deviceWebDriver.findElement(By.xpath(ele.btn_codCliente)).click();
			for(int i = 0; i < 9; i++) 
			{
				deviceWebDriver.findElement(By.xpath(ele.BTN_BORRAR_BUC)).click();
			}
			
			Thread.sleep(4000);

		} catch (Exception e) {
			System.out.println("Error en borrarBuc");
		}
	}
	
}

