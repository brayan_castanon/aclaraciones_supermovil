package mx.com.santander.suite.test.pageobject.spi;

import org.openqa.selenium.*;
import org.xframium.device.factory.DeviceWebDriver;
import org.xframium.page.AbstractPage;

import mx.com.santander.ElementosDOM;
import mx.com.santander.suite.test.pageobject.objectRepositoryStolenTDDAndroid;


public class objectRepositoryStolenTDDAndroidImpl extends AbstractPage implements objectRepositoryStolenTDDAndroid
{			
	ElementosDOM ele = new ElementosDOM();
	
	
	public void initializePage() {

	}

	
//	ACLARACIONES FLUJO Robo
	
	
	public void clickBtnClarifiMovement() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.BTC_CLARIFY_MOVEMENT)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void clickBtnRobo() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.btnStolen)).click();
			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	
	public void National() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.btnMexico)).click();
			System.out.println("---------------------------------------------------------- EL COLOR ES  ------------"+deviceWebDriver.findElement(By.xpath(ele.btnMexico)).getCssValue("color")+"--------------------------------------------------------");
			System.out.println("---------------------------------------------------------- EL TIPO DE LETRA ES  ------------"+deviceWebDriver.findElement(By.xpath(ele.btnMexico)).getCssValue("font-family")+"--------------------------------------------------------");
			System.out.println("---------------------------------------------------------- EL TEXTO ES  ------------"+deviceWebDriver.findElement(By.xpath(ele.btnMexico)).getText()+"--------------------------------------------------------");
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	
	public void State() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.STATE)).click();
			System.out.println("---------------------------------------------------------- EL COLOR ES  ------------"+deviceWebDriver.findElement(By.xpath(ele.STATE)).getCssValue("color")+"--------------------------------------------------------");
			System.out.println("---------------------------------------------------------- EL TIPO DE LETRA ES  ------------"+deviceWebDriver.findElement(By.xpath(ele.STATE)).getCssValue("font-family")+"--------------------------------------------------------");
			System.out.println("---------------------------------------------------------- EL TEXTO ES  ------------"+deviceWebDriver.findElement(By.xpath(ele.STATE)).getText()+"--------------------------------------------------------");
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	public void BtnContinuar() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.BTNCONTINUAR)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	public void Movement1() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.MOVEMENT1)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void Movement2() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.MOVEMENT2)).click();
			System.out.println("---------------------------------------------------------- EL COLOR ES  ------------"+deviceWebDriver.findElement(By.xpath(ele.MOVEMENT2)).getCssValue("color")+"--------------------------------------------------------");
			System.out.println("---------------------------------------------------------- EL TIPO DE LETRA ES  ------------"+deviceWebDriver.findElement(By.xpath(ele.MOVEMENT2)).getCssValue("font-family")+"--------------------------------------------------------");
			System.out.println("---------------------------------------------------------- EL TEXTO ES  ------------"+deviceWebDriver.findElement(By.xpath(ele.MOVEMENT2)).getText()+"--------------------------------------------------------");
			
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void BtnContinuar2() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.BTNCONTINUAR2)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void LblInfoBancaria() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(10000);
			deviceWebDriver.findElement(By.xpath(ele.INFOBANCARIA)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	public void CloseReceipt() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.CLOSERECEIPT)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void foreign() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.btnForeign)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
		
}