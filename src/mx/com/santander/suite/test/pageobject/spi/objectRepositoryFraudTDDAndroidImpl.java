package mx.com.santander.suite.test.pageobject.spi;

import org.openqa.selenium.By.*;
import org.openqa.selenium.*;
import org.xframium.Initializable;
import org.xframium.device.factory.DeviceWebDriver;
import org.xframium.page.AbstractPage;
import org.xframium.page.ElementDescriptor;
import org.xframium.page.element.Element;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;

import cucumber.api.java.en.When;
import mx.com.santander.ElementosDOM;
import mx.com.santander.suite.test.pageobject.objectRepositoryInicioSesionAndroid;
import mx.com.santander.suite.test.pageobject.objectRepositoryFraudTDDAndroid;

public class objectRepositoryFraudTDDAndroidImpl extends AbstractPage implements objectRepositoryFraudTDDAndroid
{			
	ElementosDOM ele = new ElementosDOM();
	
	
	public void initializePage() {

	}

	
//	ACLARACIONES FLUJO FRAUDE
	
	public void clickTarjetaTDD() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.RESUMEN_TDD)).click();
			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void clickBtnClarifiMovement() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.BTC_CLARIFY_MOVEMENT)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void LabelFraude() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.LABEL_FRAUDE)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	public void National() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.NATIONAL)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void Foreign() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.FOREIGN)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	public void State() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.STATE)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	public void BtnContinuar() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.BTNCONTINUAR)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	public void Movement1() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.MOVEMENT1)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void Movement2() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.MOVEMENT2)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	
	public void BtnContinuar2() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.BTNCONTINUAR2)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	
	
	public void LblInfoBancaria() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(10000);
			deviceWebDriver.findElement(By.xpath(ele.INFOBANCARIA)).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	public void CloseReceipt() throws Exception {

		DeviceWebDriver deviceWebDriver = null;

		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			deviceWebDriver.findElement(By.xpath(ele.CLOSERECEIPT)).click();
//			Thread.sleep(20000);
		} catch (Exception e) {
			System.out.println("Existe un error con el elemento");
		} 
	}
	
	
		
}
