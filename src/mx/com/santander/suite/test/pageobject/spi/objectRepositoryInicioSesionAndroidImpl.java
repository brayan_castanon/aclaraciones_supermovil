package mx.com.santander.suite.test.pageobject.spi;



import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import javax.crypto.Mac;

import org.xframium.Initializable;
import org.xframium.device.factory.DeviceWebDriver;

import java.io.*;

import org.apache.commons.collections.HashBag;
import org.apache.poi.hssf.usermodel.HSSFWorkbook; 
import org.apache.poi.ss.usermodel.Cell; 
import org.apache.poi.ss.usermodel.Row; 
import org.apache.poi.ss.usermodel.Sheet; 
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.By;
import org.xframium.page.AbstractPage;

import com.gargoylesoftware.htmlunit.javascript.HtmlUnitScriptable;
import com.gargoylesoftware.htmlunit.javascript.host.Map;

import mx.com.santander.suite.libraries.ManagementExcel;
import mx.com.santander.suite.test.pageobject.objectRepositoryInicioSesionAndroid;
import net.sourceforge.htmlunit.corejs.javascript.Scriptable;
import net.sourceforge.htmlunit.corejs.javascript.ScriptableObject;



public class objectRepositoryInicioSesionAndroidImpl extends AbstractPage implements objectRepositoryInicioSesionAndroid
{	
	
	
	public void initializePage()
    {
    }	
	
	public void clickIngresar() throws Exception {
		getElement(objectRepositoryInicioSesionAndroid.INGRESAR_1).click();
	}

	public void setIngresarNumCliente(String usuario) throws Exception {
		getElement(objectRepositoryInicioSesionAndroid.CODCLIENTE).setValue(usuario, Initializable.xFID.get());
	}

	public void clickContinuar() throws Exception {
		getElement(objectRepositoryInicioSesionAndroid.BTNCONTINUAR).click();
		
	}

	public void setPassword(String password) throws Exception {
		try {
		getElement(objectRepositoryInicioSesionAndroid.PASSWORD).setValue(password, Initializable.xFID.get());
		}catch (Exception e){
			System.out.println("Volviendo a lanzar la busqueda..");
			getElement(objectRepositoryInicioSesionAndroid.PASSWORD).setValue(password, Initializable.xFID.get());
		}
		
	}


	public void clickContinuar2() throws Exception {
		getElement(objectRepositoryInicioSesionAndroid.BTNCONTINUAR2).click();
		
	}
	
	public void existMiVida() throws Exception {
		DeviceWebDriver deviceWebDriver = null;
		try {
			if (webDriver instanceof DeviceWebDriver) {
				deviceWebDriver = (DeviceWebDriver) webDriver;
			}
			Thread.sleep(10000);
			deviceWebDriver.findElement(By.xpath("//*[text()=\'Transferencias frecuentes\']"));
			Thread.sleep(10000);

		}catch (Exception e) {
			System.out.println("Existe un error con el elemento en mi Vida");
	}
	}
	
	public void clickLight() throws Exception {
		getElement(objectRepositoryInicioSesionAndroid.MODLIGTH).click();
	}
	
	public void clickConfirmacionMod() throws Exception {
		getElement(objectRepositoryInicioSesionAndroid.CONFIRMACIONMOD).click();
	

	while (webDriver == null) {
		System.out.println("SI HAY MOVIMIENTOS");
		 @SuppressWarnings("deprecation")
		Scriptable clickm = (Scriptable) new HashBag();
		 ((ScriptableObject) clickm).put("label", clickm, "CERRAR");
	
	}
}	

}
