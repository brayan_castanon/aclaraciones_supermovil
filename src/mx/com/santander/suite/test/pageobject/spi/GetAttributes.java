package mx.com.santander.suite.test.pageobject.spi;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;

import org.openqa.selenium.JavascriptExecutor;


public class GetAttributes {
	
	
//		ElementsClarification elements = new ElementsClarification();
		private WebDriver driver;
		
		public GetAttributes(WebDriver driver) 
		{
			this.driver = driver;
		}
		
		public String getColorsByCss(GetAttributes att,String elemento, WebDriver driver) 
		{	
			Color color = Color.fromString(driver.findElement(By.cssSelector(elemento)).getCssValue("color"));
			return color.asHex();
		}
		
		public String getColorsByID(GetAttributes att,String elemento, WebDriver driver) 
		{	
			Color color = Color.fromString(driver.findElement(By.id(elemento)).getCssValue("color"));
			return color.asHex();
		}
		
		public String getColorBackByID(GetAttributes att,String elemento, WebDriver driver) 
		{	
			Color color = Color.fromString(driver.findElement(By.id(elemento)).getCssValue("background-color"));
			return color.asHex();
		}
		
		public String getColorBackByCss(GetAttributes att,String elemento, WebDriver driver) 
		{	
			Color color = Color.fromString(driver.findElement(By.cssSelector(elemento)).getCssValue("background-color"));
			return color.asHex();
		}
		
		public String getColorBorderByID(GetAttributes att,String elemento, WebDriver driver) 
		{	
			Color color = Color.fromString(driver.findElement(By.id(elemento)).getCssValue("border-color"));
			return color.asHex();
		}
		
		public String getColorBorderByCss(GetAttributes att,String elemento, WebDriver driver) 
		{	
			Color color = Color.fromString(driver.findElement(By.xpath(elemento)).getCssValue("border-color"));
			return color.asHex();
		}
		
		public String getFontTypeByCss(GetAttributes att,String elemento, WebDriver driver) 
		{
			
			String font_family = driver.findElement(By.cssSelector(elemento)).getCssValue("font-family");
			return font_family;
		}
		
		public String getFontTypeByID(GetAttributes att,String elemento, WebDriver driver) 
		{

				String prb1 = driver.findElement(By.id(elemento)).getCssValue("font-family");
				return prb1;
		}
		
		public String getTextById(GetAttributes att,String elemento, WebDriver driver) 
		{

				String prb1 = driver.findElement(By.id(elemento)).getText();
				return prb1;
		}
		
		public String getTextByCss(GetAttributes att,String elemento, WebDriver driver) 
		{

				String prb1 = driver.findElement(By.cssSelector(elemento)).getText();
				return prb1;
		}
		
		//identificar elementos con class
		
		public String getTextByClass(GetAttributes att,String elemento, WebDriver driver) 
		{

				String prb1 = driver.findElement(By.className(elemento)).getText();
				return prb1;
		}
		
		
		
		
		public String compara(String attrib1, String attrib2) throws Exception 
		{
			
			int res = attrib1.compareTo(attrib2);
			String resultado = null;
			
			if(res == 0) 
			{
				System.out.println("El valor obtenido es: " + attrib1 + " El valor esperado es: " + attrib2);
				resultado = "El resultado de la comparacion es EXITOSA: " + "Se obtuvo: " + attrib1 + " Se esperaba: " + attrib2;
			}else 
			{
				System.out.println("El valor obtenido es: " + attrib1 + " El valor esperado es: " + attrib2);
				resultado = "El resultado de la comparacion es FALLIDA: " + "Se obtuvo: " + attrib1 + " Se esperaba: " + attrib2;
			}
			return resultado;
		}
		
		public String getPlaceHolderId(GetAttributes att,String elemento, WebDriver driver) 
		{
				String prb1 = driver.findElement(By.id(elemento)).getAttribute("placeholder");

				return prb1;
		}
		
		public String getBackgroundId(GetAttributes att,String elemento, WebDriver driver) 
		{
			Color color = Color.fromString(driver.findElement(By.id(elemento)).getCssValue("background-color"));
			
			return color.asHex();

		}
		
		public String getBackgroundxPath(GetAttributes att,String elemento, WebDriver driver) 
		{
			Color color = Color.fromString(driver.findElement(By.xpath(elemento)).getCssValue("background-color"));
			
			return color.asHex();

		}
		
		public String getTextByXpath(GetAttributes att,String elemento, WebDriver driver)
		{
			String text = driver.findElement(By.xpath(elemento)).getText();
			return text;
			
		}
		
		public String getColorsByXpath(GetAttributes att,String elemento, WebDriver driver) 
		{	
			Color color = Color.fromString(driver.findElement(By.xpath(elemento)).getCssValue("color"));
			return color.asHex();
		}
		
		public String getFontTypeByXpath(GetAttributes att,String elemento, WebDriver driver) 
		{
			
			String font_family = driver.findElement(By.xpath(elemento)).getCssValue("font-family");
			return font_family;
		}
		
		public String getBackgroundCss(GetAttributes att,String elemento, WebDriver driver) 
		{
			Color color = Color.fromString(driver.findElement(By.cssSelector(elemento)).getCssValue("background-color"));
			
			return color.asHex();

		}

}

