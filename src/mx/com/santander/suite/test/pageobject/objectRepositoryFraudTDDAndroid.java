package mx.com.santander.suite.test.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import io.appium.java_client.android.AndroidDriver;

import org.xframium.Initializable;
import org.xframium.device.factory.DeviceWebDriver;
import org.xframium.page.Page;
import org.xframium.page.Page.ElementDefinition;

public interface objectRepositoryFraudTDDAndroid extends Page

{	

	
	
	@ElementDefinition
	public static final String CLICKTARJETATDD = "clickTarjetaTDD";
	
//	Aclaraciones project
	
	public void clickTarjetaTDD() throws Exception;
	public void clickBtnClarifiMovement() throws Exception;
	
	public void LabelFraude() throws Exception;
	public void National() throws Exception;
	public void Foreign() throws Exception;
	public void State() throws Exception;
	public void BtnContinuar() throws Exception;
	public void Movement1() throws Exception;
	public void Movement2() throws Exception;
	public void BtnContinuar2() throws Exception;
	public void LblInfoBancaria() throws Exception;
	
	public void CloseReceipt() throws Exception;
	

	

}