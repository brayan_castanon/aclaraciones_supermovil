package mx.com.santander.suite.libraries;

	import java.io.File;
	import java.io.IOException;
	import java.util.ArrayList;
	import java.util.List;

	import jxl.Sheet;
	import jxl.Workbook;
	import jxl.read.biff.BiffException;
import mx.com.santander.suite.test.Prueba;
import mx.com.santander.suite.test.pageobject.spi.objectRepositoryInicioSesionAndroidImpl;

	public class ManagementExcel {
		
		/**
		 * Metodo que obtiene una lista de cadenas a partir de los valores de una columna de excel.
		 * @param cellContent
		 * @param file
		 * @return list<String>
		 */
		public List<String> getColumn(String cellContent, String file){
			List<String> column = null;
			int columnPosition = 0;
			int positionFirstRow = 0;
			
			try {
			List<String> row = getRow(positionFirstRow, file);
			
			if(row.contains(cellContent)) {
				int cont = 0;
				for(String cell: row) {
					if(cell.equalsIgnoreCase(cellContent)) {
						columnPosition = cont;					
					}
					cont++;
				}	
				
				
				column = new ArrayList<String>();
				
				Workbook workbook = Workbook.getWorkbook(new File(file));
				Sheet sheet = workbook.getSheet(0);//Hoja que se va a leer				
					
					for (int fila = 0; fila < sheet.getRows(); fila++) { //recorremos las filas
						String cell = sheet.getCell(columnPosition, fila).getContents();
						if(!cell.isEmpty()) {
							column.add(cell);
						}
					}			
				}
			}catch( Exception e) {
				e.printStackTrace();
			}
		
			
			return column;
		}//Fin metodo getColumn
		
		/**
		 * Metodo que obtiene una lista con el contenido de una fila.
		 * 
		 * @param indiceFila
		 * @return fila
		 */
		public List<String> getRow(int indexRow, String file){
			List<String> row = null;
			
			try {
				row = new ArrayList<String>();
				
				Workbook workbook = Workbook.getWorkbook(new File(file));
				Sheet sheet = workbook.getSheet(0);//Hoja que se va a leer
				
				for (int columna = 0; columna < sheet.getColumns(); columna++) { //recorremos las columnas					
					for (int fila = 0; fila < sheet.getRows(); fila++) { //recorremos las filas
						if(fila == indexRow) {
							row.add(sheet.getCell(columna, fila).getContents());
						}
					
					}
				}
			
			}catch( IOException | BiffException e) {
				e.printStackTrace();
			}
			
			return row;
		}//Fin metodo getRow.
		
		
	public static void main(String[] args) throws Exception {
			
		
		ManagementExcel managementExcel = new ManagementExcel();
		
		List<String> filas = managementExcel.getRow(0, "C:\\Users\\Z060612.GFSSCORP\\git\\mayox2\\automatizar-apk-e-ipa\\resources\\data\\data.xls");
		
		int cont = 0;
		if(filas!=null) {
		for(String cell: filas) {
			System.out.println("El dato de la fila " + cont +" es: " + cell);
			cont++;
			}
		}else {
			System.out.println("El encabezado no contiene la cadena ingresada 2");
		}
		
		List<String> columnas = managementExcel.getColumn("Validacion", "C:\\Users\\Z060612.GFSSCORP\\git\\mayox2\\automatizar-apk-e-ipa\\resources\\data\\data.xls");
		
		System.out.println(columnas);
		cont = 0;
		if (columnas!=null) { 
		for(String cell: columnas) {
			System.out.println("El dato " + cont +" de la columna " + columnas.get(0) +" es: " + cell);
			cont++;
			}
		}else {
			System.out.println("El encabezado no contiene la cadena ingresada");
		}
		
		
		}	
			
	}

