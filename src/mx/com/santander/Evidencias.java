package mx.com.santander;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBody;

import com.sun.mail.imap.Rights.Right;

public class Evidencias {
	
	private static String ArchivoTestJS = "Test.js";
	private static String folder = "artifacts";
	private static String directorioRaiz;
	private static String NameCP;
	private static String rutaUltimaEjecucion;
    public static  String SOURCE_FILE;
	public static  String Plataforma;
	public static String varsplit; 
	public static String varparts;


	

	public static void main(String[] args) throws IOException, InvalidFormatException, XmlException {
	
		ejecutar();	 
		
	}
	
	public static void ejecutar() throws FileNotFoundException, IOException, InvalidFormatException, XmlException
	{   
		Properties p = new Properties();

if(System.getProperty("os.name").toLowerCase().contains("win")){ p.load(new FileReader("resources\\properties\\configTest.properties")); }
								 else { p.load(new FileReader("resources/properties/configTest.properties")); }

		

		directorioRaiz=p.getProperty("pathResultTest");	
		SOURCE_FILE=p.getProperty("pathTemplate");
		Plataforma=p.getProperty("plataforma");	
		
//	System.out.println("dir raiz: " + directorioRaiz);
//	System.out.println("template: " + SOURCE_FILE);
//	System.out.println("plataforma: " + Plataforma);
		
if(System.getProperty("os.name").toLowerCase().contains("win"))
	{ 
        if(SOURCE_FILE.contains("/") || directorioRaiz.contains("/"))
		{ SOURCE_FILE = SOURCE_FILE.replace("/","\\");
	      directorioRaiz = directorioRaiz.replace("/","\\");
	    }
	}
else
	{ 
	
       if(SOURCE_FILE.contains("\\") || directorioRaiz.contains("\\"))
		{ SOURCE_FILE = SOURCE_FILE.replace("\\","/");
	      directorioRaiz = directorioRaiz.replace("\\","/");
	    }

}
		
			
		lastEjecution(directorioRaiz);
		 
		
		Evidencias evidencias = new Evidencias();
		File directoryFile = new File(directorioRaiz);
		evidencias.listarArchivosEnDirectorio(directoryFile);
	}
	
	public static void lastEjecution(String dirRaiz)
	{
		int i;
		  String dir=dirRaiz, tmp=null;
		  long time=0;
		  File tempDir, tempFile;
		  Vector fileQ=new Vector();
		  fileQ.add(dir);
		  while(!fileQ.isEmpty()) 
		  {
		    tempDir=new File((String)fileQ.remove(0));
		    if(tempDir.list()!=null)
		    for(i=0; i<tempDir.list().length; i++) 
		    {
		      tempFile=new File(tempDir.getPath()+File.separatorChar+tempDir.list()[i]);
		         Pattern pat = Pattern.compile(".*-.*");
			     Matcher mat = pat.matcher(tempFile.getName());
		      
			     if(tempFile.isDirectory() && mat.matches() ) 
			     {
		           if(tempFile.lastModified()>time) 
		           { time=tempFile.lastModified();     tmp=tempFile.getPath(); 
		           }
		           fileQ.add(tempFile.getPath());
//		           System.out.println("dentro if" + tempFile.getPath());
		         }
		    }
		  }
	
      rutaUltimaEjecucion=tmp;
//      System.out.println("ruta" + rutaUltimaEjecucion);
	}	
	
	
	public void listarArchivosEnDirectorio(File directorio) throws InvalidFormatException, FileNotFoundException, IOException, XmlException {
		
		XWPFDocument template = new XWPFDocument(new FileInputStream(SOURCE_FILE));
	    XWPFParagraph par = template.createParagraph();
	    XWPFRun run = par.createRun();
	      
	        
	    List<String> ProyectDataInTest = null;
		List<String> imagesInTest = null;
		File[] listadoArchivos = directorio.listFiles();
		
		if (listadoArchivos != null) {
			for (File archivo : listadoArchivos) 
			{	if (archivo.isDirectory()) 
				{	listarArchivosEnDirectorio(archivo);
				} else 
				{	if (archivo.getName().equalsIgnoreCase(ArchivoTestJS))
	
					{	
					
					  if(archivo.getParent().contains(rutaUltimaEjecucion))
					     {
					
					imagesInTest = findImagesInTest(archivo); 
					ProyectDataInTest=findProyectData(archivo);
					
						if(!imagesInTest.isEmpty())
					    
						{
							NameCP=(archivo.getParent()+".docx");
							NameCP=NameCP.replace(directorioRaiz.toString(), "FOR_DES_502");
							
										if(NameCP.contains("\\") || NameCP.contains("/"))
										{ NameCP = NameCP.replace("\\","_");
									      NameCP = NameCP.replace("/","_");
										}
									
				 
//						    System.out.println("NameCP "+NameCP);
						   
						   
							for (String imagenInTest : imagesInTest) 
							{
								
							System.out.println("imagenInTest"+imagenInTest);
								
							    FileInputStream pic = new FileInputStream(imagenInTest);
								
								if(Plataforma.contains("Web")){
									run.addPicture(pic, Document.PICTURE_TYPE_JPEG, "2", Units.toEMU(500), Units.toEMU(200)); 
									}
								if(Plataforma.contains("Movil")){
									run.addPicture(pic, Document.PICTURE_TYPE_JPEG, "2", Units.toEMU(200), Units.toEMU(350)); 
									}
					            pic.close();    
							}
							
							CTBody body = template.getDocument().getBody();
						    String templateContent = body.xmlText();
						    List<String> ch = null;
						    
							 if(!ProyectDataInTest.isEmpty())
								{
								 
															
									for (String DataInTest : ProyectDataInTest) 
									{	
										
//										System.out.println("DataInTest for" + DataInTest);	
									 
										if(DataInTest.contains("Tester:")) {
									    templateContent = templateContent.replace("tester_ex", DataInTest.replace("Tester:", "")); 
									    System.out.println("DataInTest if" + templateContent);
										}
										if(DataInTest.contains("Clave Proyecto:")) {
										    templateContent = templateContent.replace("clave_proyecto", DataInTest.replace("Clave Proyecto:", "")); 
										}
										if(DataInTest.contains("Nombre Proyecto:")) {
										    templateContent = templateContent.replace("nomproy", DataInTest.replace("Nombre Proyecto:","")); 
										    System.out.println("DataInTest if" + DataInTest);
										}
										if(DataInTest.contains("dateOnly:")) {
										    templateContent = templateContent.replace("varDate", DataInTest.replace("dateOnly:","")); 
										    System.out.println("DataInTest if" + DataInTest);
										}
										
										if(DataInTest.contains("timeOnly:")) {
										    templateContent = templateContent.replace("varHra", DataInTest.replace("timeOnly:","")); 
										    System.out.println("DataInTest if" + DataInTest);
										}

                                        if(DataInTest.contains("XF_TEST_STATUS:")) {
										    templateContent = templateContent.replace("est_eje", DataInTest.replace("XF_TEST_STATUS:","")); 
										    System.out.println("DataInTest if" + DataInTest);
                                        }
										
										if(DataInTest.contains("Responsable del Cambio:")) {
										    templateContent = templateContent.replace("varRC", DataInTest.replace("Responsable del Cambio:","")); 
										    System.out.println("DataInTest if" + DataInTest);
										}

										if(DataInTest.contains("iOSUsuario:")) {
										    templateContent = templateContent.replace("iOSUsuario", DataInTest.replace("iOSUsuario:","")); 
										    System.out.println("DataInTest if" + DataInTest);
										}
	
										if(DataInTest.contains("Ciclo Pruebas:")) {
										    templateContent = templateContent.replace("varcp", DataInTest); 
										    System.out.println("DataInTest if" + DataInTest);
										}
										
										
									}
									
								
								}
						
							
								templateContent = templateContent.replace("caso_prueba", NameCP); 
						    	
						    	 body.set(CTBody.Factory.parse(templateContent));
								 
								 if(System.getProperty("os.name").toLowerCase().contains("win")){ template.write(new FileOutputStream(new File(archivo.getParent()+"\\"+NameCP))); }
								 else { template.write(new FileOutputStream(new File(archivo.getParent()+"/"+NameCP))); }
									 
						     
						}
						
					}
				
				}
			  }
			}
		}
		
	}
	
	
	
	
	private static List<String> findImagesInTest(File testJS) throws FileNotFoundException
	{	
	
		Scanner scannerTestJS = new Scanner(new File("" + testJS.getPath() + ""));

		List<String> allImages = findImages(new File(directorioRaiz), folder);
		
		List<String> imagesInTest = new ArrayList<String>();
		
		while (scannerTestJS.hasNextLine()) 
		{	
			String lineaTestJS = scannerTestJS.nextLine();
			
			
			if (allImages.isEmpty()) 
			{	return imagesInTest;
			}
				
			for (String image : allImages) 
			{
				String string = image;
				
				
				
				if(System.getProperty("os.name").toLowerCase().contains("win")){ varparts= ("\\\\artifacts\\\\"); }
				else { varparts= ("/artifacts/"); }
				
				String[] parts = string.split(varparts);
				
				String part2 = parts[1]; 

				if (lineaTestJS.indexOf(part2) >=0 && !imagesInTest.contains(image)) 
				{	
					
					imagesInTest.add(image);
				
			   
					break;
				}
			}
		}
		
		return imagesInTest;
		
	}
	
	



	private static List<String> findProyectData(File testJS) throws FileNotFoundException
	{	
	
		Scanner scannerTestJSData = new Scanner(new File("" + testJS.getPath() + ""));
//		System.out.println("aqui esta: " + testJS.getPath());
		List<String> ProyectData = new ArrayList<String>();
		List<String> ProyectDataInTest = new ArrayList<String>();
		List<String> ProyectDataInTestCadena = new ArrayList<String>();
		
		ProyectData.add("clave_proyecto");
		ProyectData.add("nombre_proyecto");
		ProyectData.add("tester");
		ProyectData.add("dateOnly");
		ProyectData.add("timeOnly");
        ProyectData.add("XF_TEST_STATUS");
		ProyectData.add("responsable_cambio");
		ProyectData.add("ciclo");
		ProyectData.add("iOSUsuario");
		
		
		while (scannerTestJSData.hasNextLine()) 
		{	
			String lineaTestJSData = scannerTestJSData.nextLine();
//			System.out.println("sacaner "  + lineaTestJSData);
			
			if (ProyectData.isEmpty()) 
			{	return ProyectDataInTest;
			}
			
			for (String dato : ProyectData) 
			{
							
				if (lineaTestJSData.indexOf(dato) >=0 && !ProyectDataInTest.contains(dato)) 
				{	
					
					ProyectDataInTest.add(dato);
					
					if(System.getProperty("os.name").toLowerCase().contains("win")){ lineaTestJSData=lineaTestJSData.replace("\"", ""); }
				else { lineaTestJSData=lineaTestJSData.replace("\"", ""); }
					
					lineaTestJSData=lineaTestJSData.replace(",", "");
					
					if(dato=="tester") {lineaTestJSData=lineaTestJSData.replace("tester:", "");
					}
					
					if(dato=="clave_proyecto") {lineaTestJSData=lineaTestJSData.replace("clave_proyecto:", "");}
					
					if(dato=="nombre_proyecto") {lineaTestJSData=lineaTestJSData.replace("nombre_proyecto:", "");}
					
					if(dato=="responsable_cambio") {lineaTestJSData=lineaTestJSData.replace("responsable_cambio:", "");}
					
					if(dato=="ciclo") {lineaTestJSData=lineaTestJSData.replace("ciclo:", "");}
					
					if(dato=="iOSUsuario") {lineaTestJSData=lineaTestJSData;}

					if(dato=="dateOnly") {lineaTestJSData=lineaTestJSData;}
					
					if(dato=="timeOnly") {lineaTestJSData=lineaTestJSData;}
					
                    if(dato=="XF_TEST_STATUS") {lineaTestJSData=lineaTestJSData;}
					
					ProyectDataInTestCadena.add(lineaTestJSData);
					
					break;
				}
		
			}
		
		
	    }
		return ProyectDataInTestCadena;
	}
	

	
	
private static List<String> findImages(File root, String name) 
	{	
	
		ArrayList<String> lstNombreImagenes = new ArrayList<String>();
		
		File[] files = root.listFiles();

		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
                
				if(System.getProperty("os.name").toLowerCase().contains("win")){ varsplit= ("\\\\"); }
				else { varsplit= ("/");  }
				
				String split[] = files[i].toString().split(varsplit);
				
					int splitcount = split.length - 1;

					if (split[splitcount].equals(name)) {
						
					String	lstrutaImagenes=files[i].toString();
						
						
						File directoryImg = new File(files[i].toString());

						File[] imagenes = directoryImg.listFiles(new FilenameFilter() {
							@Override
							public boolean accept(File dir, String name) {
								return name.toLowerCase().contains("state");
								
							}
						});

						for (File file : imagenes) {
						
                if(System.getProperty("os.name").toLowerCase().contains("win")){ lstNombreImagenes.add(lstrutaImagenes.replace("\\","\\\\")+"\\\\"+file.getName()); }
				else { lstNombreImagenes.add(lstrutaImagenes.replace("//","/")+"/"+file.getName()); }	
				
						
							
						}
					}
					lstNombreImagenes.addAll(findImages(files[i], name));

				}
			}
		}
		return lstNombreImagenes;
	}

}

